unit DataTypes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, DateUtils, XML.VerySimple;

const
  nl=#13#10; dnl=nl+nl;
  NeccFolders: array[0..3] of string=('data', 'data\backups', 'music', 'utilities');
  NeccFiles: array[0..3] of string=('data\teams.xml', 'data\groups.xml', 'utilities\bgimg0.jpg', 'utilities\bgimg1.jpg');

  ne = 16;
  ng =  4;
  nv =  8;
  nm = 31;

type TCompetition=(CUnknown, CEuro, CWorldCup);
     TCompetitionResult=(CRUnknown, CRWinner, CRRunnerUp, CRBronze, CRQuarterFinals);

type TTeam=record
  Name, ShortName, Nickname, Color1, Color2: string;
end;

type TGroupPoz=record
  TeamID: string;
  nmp, nv, ne, nd, gf, ga, gol, pts: integer;
end;

type TGroup=record
  Name: string;
  Poz: array[0..3] of TGroupPoz;
  nGM: integer;
end;

type TVenue=record
  Name, City, Country: string;
  Capacity: longint;
end;

type TMatch=record
  Disputed: boolean;
  Stage, City, Team1, Team2, ESMTeam1, ESMTeam2: string;
  When: TDateTime;
  Goals1, Goals2: byte;
  MatchResultType: string;
end;

var
  t: array of TTeam;
  g: array of TGroup;
  v: array of TVenue;
  m: array of TMatch;

  x: TVerySimpleXML;

  f: textfile;

  procedure log(s: string);
  procedure XMLtoData(typ: string; x: TVerySimpleXML);
  procedure SaveDataToXML(typ: string);
  procedure ReadData;
  procedure RecalculateGroups;
  procedure ProcessEliminatoryStage;
  procedure scan_songs(directory: string; var list: TStringList);

implementation

uses Euro12_pas, functii;

procedure log(s: string);
var logf: textfile;
begin
  try
    F1.logL.Caption:=s+formatdatetime(' (d mmm yy hh:nn)', now);
    assignfile(logf, 'data\log.txt'); append(logf); writeln(logf, formatdatetime('dd.mmm.yyyy hh:nn:ss    |    ', now), s); closefile(logf)
  except
    begin
      try
        F1.logL.Caption:=s+formatdatetime('"(Log retry, "d mmm yy hh:nn)', now);
        assignfile(logf, 'data\log.txt'); append(logf); writeln(logf, formatdatetime('dd.mmm.yyyy hh:nn:ss    |    "Log retry: "', now), s); closefile(logf)
      except on E:Exception do showmessage('Failed a second time to log message "'+s+'"'+dnl+e.classname+' :  '+e.Message) end
    end
  end
end;

procedure XMLtoData(typ: string; x: TVerySimpleXML);
var i, j: word; xs: string; xn: TXmlNode;
begin
  log('XMLtoData(typ="'+typ+'")');
  case strtocase(typ, ['teams', 'groups', 'venues', 'matches']) of
  1:
    begin try
      setlength(t, ne);
      for i:=0 to ne-1 do
        begin
          try t[i].Name := x.Root.ChildNodes[i].Find('name').Text except t[i].Name:='unknown' end;
            log('Extracting info for team i='+inttostr(i)+' = "'+t[i].Name+'"');
          try t[i].ShortName := x.Root.ChildNodes[i].Find('shortname').Text except t[i].ShortName:='UNK' end;
          try t[i].Nickname := x.Root.ChildNodes[i].Find('nickname').Text except t[i].Nickname:='none/unknown' end;
          try t[i].Color1 := x.Root.ChildNodes[i].Find('color1').Text except t[i].Color1:='#808080' end;
          try t[i].Color2 := x.Root.ChildNodes[i].Find('color2').Text except t[i].Color2:='#808080' end
        end;
      log('XMLtoData (teams) success')
    except on E:Exception do showmessage('XMLtoData ERROR while converting to "'+typ+'"'+dnl+E.ClassName+': '+E.Message) end end;
  2:
    begin try
      setlength(g, ng);
      for i:=0 to ng-1 do
        begin
          xn := x.Root.Find('group', 'id', chr(65+i)); g[i].Name:=chr(65+i);
          for j:=0 to 3 do
            begin
              try g[i].Poz[j].TeamID := xn.Find('team', 'seedPot', inttostr(j+1)).Text except g[i].Poz[j].TeamID:='UNK' end;
              log('Group "'+g[i].Name+'", team j='+inttostr(j)+' = "'+g[i].Poz[j].TeamID+'"')
            end;
        end;
      log('XMLtoData (groups) success')
    except on E:Exception do showmessage('XMLtoData ERROR while converting to "'+typ+'"'+dnl+E.ClassName+': '+E.Message) end end;
  3:
    begin try
      setlength(v, nv);
      for i:=0 to nv-1 do
        begin
          try v[i].name := x.Root.ChildNodes[i].Find('name').Text except v[i].name:='unknown' end;
          try v[i].city := x.Root.ChildNodes[i].Find('city').Text except v[i].city:='unknown' end;
          try v[i].country := x.Root.ChildNodes[i].Find('country').Text except v[i].country:='unknown' end;
          try v[i].capacity := strtoint(x.Root.ChildNodes[i].Find('capacity').Text) except v[i].capacity:=0 end;
          log('Venue "'+v[i].name+'" in "'+v[i].city+'", "'+v[i].country+'", capacity='+inttostr(v[i].capacity))
        end;
      log('XMLtoData (venues) success')
    except on E:Exception do showmessage('XMLtoData ERROR while converting to "'+typ+'"'+dnl+E.ClassName+': '+E.Message) end end;
  4:
    begin try
      setlength(m, nm);
      for i:=0 to nm-1 do
        begin
          xn := x.Root.Find('match', 'id', inttostr(i));
          try m[i].Stage := xn.Attribute['stage'] except m[i].Stage:='unknown' end;
          try m[i].When := StrToWhen(xn.Find('date').Text) except m[i].When:=encodedatetime(1900, 1, 1, 0, 0, 0, 0) end;
          try m[i].City := xn.Find('city').Text except m[i].City:='unknown' end;
          try xs := ansiuppercase(xn.Attribute['ESMTeams']); m[i].ESMTeam1:=xs[1]+xs[2]; m[i].ESMTeam2:=xs[4]+xs[5] except m[i].ESMTeam1:=''; m[i].ESMTeam2:='' end;
          try m[i].Team1 := xn.Find('team1').Text except m[i].Team1:='UNK' end;
          try m[i].Team2 := xn.Find('team2').Text except m[i].Team2:='UNK' end;
          try m[i].Disputed := xn.Find('disputed')<>nil except m[i].Disputed:=false end;
          try m[i].MatchResultType := xn.Find('disputed').Attribute['result_type'] except m[i].MatchResultType:='normal' end;
          if m[i].Disputed then
            begin
              try
                m[i].Goals1 := strtoint(xn.Find('disputed').Find('goals1').Text); m[i].Goals2 := strtoint(xn.Find('disputed').Find('goals2').Text)
              except m[i].Goals1:=0; m[i].Goals2:=0 end;
            end;
          log('Match i='+inttostr(i)+' (stage "'+m[i].Stage+'"), between "'+m[i].Team1+'" and "'+m[i].Team2+'", in "'+m[i].City+'", on '+formatdatetime('ddd, d mmm yyyy, h:nn', m[i].When)+' (disputed='+booleantoyesno(m[i].Disputed)+', '+m[i].MatchResultType+', '+inttostr(m[i].Goals1)+'-'+inttostr(m[i].Goals2)+')')
        end;
      log('XMLtoData (matches) success')
    except on E:Exception do showmessage('XMLtoData ERROR while converting to "'+typ+'"'+dnl+E.ClassName+': '+E.Message) end end
  else showmessage('XMLtoData ERROR: unknown typ="'+typ+'"')
  end
end;

procedure SaveDataToXML(typ: string);
var i: word;
begin
  log('SaveDataToXML(typ="'+typ+'")'); x:=TVerySimpleXML.Create;
  case strtocase(typ, ['teams', 'groups', 'venues', 'matches']) of
  1:
    begin try

      log('SaveDataToXML (teams) success')
    except on E:Exception do showmessage('SaveDataToXML ERROR while saving "'+typ+'"'+dnl+E.ClassName+': '+E.Message) end end;
  2:
    begin try

      log('SaveDataToXML (groups) success')
    except on E:Exception do showmessage('SaveDataToXML ERROR while saving "'+typ+'"'+dnl+E.ClassName+': '+E.Message) end end;
  3:
    begin try

      log('SaveDataToXML (venues) success')
    except on E:Exception do showmessage('SaveDataToXML ERROR while saving "'+typ+'"'+dnl+E.ClassName+': '+E.Message) end end;
  4:
    begin try
      if not copyfile(pwidechar('data\matches.xml'), pwidechar('data\backups\matches '+formatdatetime('yyyy-mm-dd hh.nn', now)+'.xml'), false) then
        if MessageDlg('File "macthes.xml" file backup has failed. Continue?', mtWarning, mbYesNo, 0)<>mrYes then exit;
      x.Root.NodeName:='matches';
      for i:=0 to nm-1 do
        begin
          x.Root.AddChild('match');
          x.Root.ChildNodes[i].SetAttribute('id', inttostr(i)); x.Root.ChildNodes[i].SetAttribute('stage', m[i].Stage);
          if (m[i].ESMTeam1<>'') and (m[i].ESMTeam2<>'') then begin x.Root.ChildNodes[i].SetAttribute('ESMTeams', m[i].ESMTeam1+'-'+m[i].ESMTeam2) end;
          with x.Root.ChildNodes[i] do
            begin
              AddChild('date'); ChildNodes[0].Text:=formatdatetime('yyyy/mm/dd hh:nn', m[i].When);
              AddChild('city'); ChildNodes[1].Text:=m[i].City;
              if m[i].Team1<>'UNK' then begin AddChild('team1'); ChildNodes[2].Text:=m[i].Team1 end;
              if m[i].Team2<>'UNK' then begin AddChild('team2'); Find('team2').Text:=m[i].Team2 end;
              if m[i].Disputed then
                begin
                  AddChild('disputed'); Find('disputed').SetAttribute('result_type', m[i].MatchResultType);
                  Find('disputed').AddChild('goals1'); Find('disputed').ChildNodes[0].Text:=inttostr(m[i].Goals1);
                  Find('disputed').AddChild('goals2'); Find('disputed').ChildNodes[1].Text:=inttostr(m[i].Goals2); ;
                end;
            end;
        end;
      x.SaveToFile('data\matches.xml'); log('SaveDataToXML (matches) success')
    except on E:Exception do showmessage('SaveDataToXML ERROR while saving "'+typ+'", i='+inttostr(i)+';'+dnl+E.ClassName+': '+E.Message) end end
  else showmessage('SaveDataToXML ERROR: unknown typ="'+typ+'"')
  end
end;

procedure ReadData;
var i: word;
begin
  log('Initializing and reading data...');
  for i:=0 to length(NeccFolders)-1 do if not directoryexists(NeccFolders[i]) then begin showmessage('The folder "'+NeccFolders[i]+'" does not exist. That is abig error.'+dnl+'You probably have a faulty installation, so reinstall. The app will now close.'); Halt end;
  for i:=0 to length(NeccFiles)-1 do if not fileexists(NeccFiles[i]) then begin showmessage('The file "'+NeccFiles[i]+'" does not exist. That is abig error.'+dnl+'You probably have a faulty installation, so reinstall. The app will now close.'); Halt end;
  assignfile(f, 'data\log.txt'); rewrite(f); closefile(f);
  DecimalSeparator:='.'; ThousandSeparator:=','; DateSeparator:='/'; TimeSeparator:=':';
  //
  try
  x:=TVerySimpleXML.Create;
  x.LoadFromFile('data\teams.xml');   XMLtoData('teams', x);
  x.LoadFromFile('data\groups.xml');  XMLtoData('groups', x);
  x.LoadFromFile('data\venues.xml');  XMLtoData('venues', x);
  x.LoadFromFile('data\matches.xml'); XMLtoData('matches', x);
  log('Data read successfully!')
  except on E:Exception do showmessage('ReadData ERROR'{ while converting to "'+typ+'"'}+dnl+E.ClassName+': '+E.Message) end
end;

procedure RecalculateGroups;
  {*}procedure ResetGroupPoz(var x: TGroupPoz);
  begin
    x.nmp:=0; x.nv:=0; x.ne:=0; x.nd:=0; x.gf:=0; x.ga:=0; x.gol:=0; x.pts:=0
  end;
  {*}procedure GroupMatchHappened(xGroup, xMatch: word);
    {*}procedure GetTempTeamGroupPositions(xG, xM: word; var P1, P2: word);
    var i: word;
    begin for i:=0 to 3 do begin if g[xG].Poz[i].TeamID=m[xM].Team1 then P1:=i; if g[xG].Poz[i].TeamID=m[xM].Team2 then P2:=i end end;
  var px, py: word;
  begin
    if not m[xMatch].Disputed then exit; GetTempTeamGroupPositions(xGroup, xMatch, px, py); //showmessage('match '+inttostr(xMatch)+' ('+m[xMatch].Team1+'-'+m[xMatch].Team2+') winner='+MatchWinner(xMatch));
    if MatchWinner(xMatch)=g[xGroup].Poz[px].TeamID then begin inc(g[xGroup].Poz[px].nv); inc(g[xGroup].Poz[py].nd) end
    else if MatchWinner(xMatch)=g[xGroup].Poz[py].TeamID then begin inc(g[xGroup].Poz[py].nv); inc(g[xGroup].Poz[px].nd) end
    else begin inc(g[xGroup].Poz[px].ne); inc(g[xGroup].Poz[py].ne) end;
    g[xGroup].Poz[px].nmp:=g[xGroup].Poz[px].nv+g[xGroup].Poz[px].ne+g[xGroup].Poz[px].nd; g[xGroup].Poz[py].nmp:=g[xGroup].Poz[py].nv+g[xGroup].Poz[py].ne+g[xGroup].Poz[py].nd;
    inc(g[xGroup].Poz[px].gf, m[xMatch].Goals1); inc(g[xGroup].Poz[px].ga, m[xMatch].Goals2); g[xGroup].Poz[px].gol:=g[xGroup].Poz[px].gf-g[xGroup].Poz[px].ga;
    inc(g[xGroup].Poz[py].gf, m[xMatch].Goals2); inc(g[xGroup].Poz[py].ga, m[xMatch].Goals1); g[xGroup].Poz[py].gol:=g[xGroup].Poz[py].gf-g[xGroup].Poz[py].ga;
    g[xGroup].Poz[px].pts:=g[xGroup].Poz[px].nv*3+g[xGroup].Poz[px].ne; g[xGroup].Poz[py].pts:=g[xGroup].Poz[py].nv*3+g[xGroup].Poz[py].ne;
    inc(g[xGroup].nGM)
  end;
  {*}function ShouldSwitchPlaces(xGroup, A, B: word): boolean;
  // http://en.wikipedia.org/wiki/Euro_2012#Group_stage
  begin
    result:=false;
    with g[xGroup] do
      begin
        if Poz[A].pts<Poz[B].pts then begin result:=true; exit end;
        {}if (Poz[A].pts=Poz[B].pts) and (Poz[A].gol<Poz[B].gol) then begin result:=true; exit end;
        {}if (Poz[A].pts=Poz[B].pts) and (Poz[A].gol=Poz[B].gol) and (Poz[A].gf<Poz[B].gf) then begin result:=true; exit end;
        log('Sorting may not be correct for teams tied by the number of points in group matches!');
        //they are tied; let's find the direct match and see who has a) won b) more goals (?)
      end
  end;
//RecalculateGroups begins
var i, j, k: word; aux: TGroupPoz;
begin
  log('Recalculating groups...');
  for i:=0 to 3 do
    begin
      //init
      for j:=0 to 3 do begin g[i].Poz[j].TeamID:=t[i*4+j].ShortName; g[i].nGM:=0; ResetGroupPoz(g[i].Poz[j]) end;
      //add match results
      for j:=0 to 5 do GroupMatchHappened(i, i*2+(j div 2)*8+j mod 2);
      //sort table
      for j:=0 to 2 do for k:=j+1 to 3 do
        if ShouldSwitchPlaces(i, j, k) then
          with g[i] do begin aux:=Poz[j]; Poz[j]:=Poz[k]; Poz[k]:=aux end
    end;
end;

procedure ProcessEliminatoryStage;
var i, j, k: word; ok: boolean;
begin
  for i:=24 to nm-1 do
    if (m[i].ESMTeam1<>'') and (m[i].ESMTeam2<>'') then
      begin
        m[i].Team1:='UNK'; m[i].Team2:='UNK'; ok:=true;
        if ((charinset(m[i].ESMTeam1[1], ['0'..'9'])) and (not charinset(m[i].ESMTeam2[1], ['0'..'9']))) or ((not charinset(m[i].ESMTeam1[1], ['0'..'9'])) and (charinset(m[i].ESMTeam2[1], ['0'..'9']))) then
          begin showmessage('ProcessEliminatoryStage ERROR, match i='+inttostr(i)+': ESMTeam1[1] <> ESMTeam2[1] in type (group, match);'+nl+'ESMTeam1="'+m[i].ESMTeam1+'", ESMTeam2="'+m[i].ESMTeam2+'"'); ok:=false end;
        if not (charinset(m[i].ESMTeam1[1], ['A'..'D', '2', '3']) and charinset(m[i].ESMTeam2[1], ['A'..'D', '2', '3'])) then begin showmessage('ProcessEliminatoryStage ERROR, match i='+inttostr(i)+': invalid ESMTeam1[1] or ESMTeam2[1];'+nl+'ESMTeam1="'+m[i].ESMTeam1+'", ESMTeam2="'+m[i].ESMTeam2+'"'); ok:=false end;
        //
        if ok then
          begin
            if not charinset(m[i].ESMTeam1[1], ['0'..'9']) then //group winner/runner-up team
              begin
                k:=ord(m[i].ESMTeam1[1])-65; j:=strtoint(m[i].ESMTeam1[2])-1; if g[k].nGM=6 then m[i].Team1:=g[k].Poz[j].TeamID;
                k:=ord(m[i].ESMTeam2[1])-65; j:=strtoint(m[i].ESMTeam2[2])-1; if g[k].nGM=6 then m[i].Team2:=g[k].Poz[j].TeamID;
                if (m[i].Team1='UNK') or (m[i].Team2='UNK') then m[i].Disputed:=false
              end
            else                                                //match winner
              begin
                if m[strtoint(m[i].ESMTeam1)-1].Disputed then m[i].Team1:=MatchWinner(strtoint(m[i].ESMTeam1)-1);
                if m[strtoint(m[i].ESMTeam2)-1].Disputed then m[i].Team2:=MatchWinner(strtoint(m[i].ESMTeam2)-1);
                if (m[i].Team1='UNK') or (m[i].Team2='UNK') then m[i].Disputed:=false
              end;
          end;
      end;
end;

procedure scan_songs(directory: string; var list: TStringList);
var search: TSearchRec;
begin
  log('Scanning for songs...'); // Subdirectories
  if FindFirst(directory + '*.MP3', faDirectory, search) = 0 then
  begin
    repeat
      if (search.Name[1] <> '.') then list.Add(search.Name)
    until FindNext(search) <> 0;
    FindClose(search)
  end;
  log('Scan in "'+directory+'" successfull. NSongs = '+inttostr(list.Count))
end;

end.
