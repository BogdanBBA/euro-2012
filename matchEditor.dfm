object FME: TFME
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'FME'
  ClientHeight = 390
  ClientWidth = 607
  Color = 10249088
  Font.Charset = ANSI_CHARSET
  Font.Color = clWhite
  Font.Height = -19
  Font.Name = 'Asap'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  ScreenSnap = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 22
  object Panel9: TPanel
    Left = 8
    Top = 8
    Width = 590
    Height = 373
    BevelInner = bvSpace
    BevelOuter = bvNone
    BevelWidth = 5
    BorderWidth = 5
    TabOrder = 0
    object saveL: TLabel
      Left = 315
      Top = 315
      Width = 47
      Height = 26
      Alignment = taRightJustify
      Caption = 'Save'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -23
      Font.Name = 'Asap'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = saveLClick
      OnMouseEnter = exitBMouseEnter
      OnMouseLeave = exitBMouseLeave
    end
    object matchIDL: TLabel
      Left = 35
      Top = 32
      Width = 103
      Height = 27
      Caption = 'matchIDL'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8454143
      Font.Height = -24
      Font.Name = 'Asap'
      Font.Style = [fsItalic]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 64
      Top = 70
      Width = 132
      Height = 22
      Caption = 'Disputed match'
    end
    object Label6: TLabel
      Left = 318
      Top = 74
      Width = 251
      Height = 17
      Alignment = taRightJustify
      Caption = 'Check the box to the left to fill in data'
      Font.Charset = ANSI_CHARSET
      Font.Color = 16759295
      Font.Height = -15
      Font.Name = 'Asap'
      Font.Style = [fsItalic]
      ParentFont = False
    end
    object exitB: TPanel
      Left = 377
      Top = 303
      Width = 192
      Height = 49
      Cursor = crHandPoint
      BevelOuter = bvNone
      Caption = 'Close'
      Color = 6227264
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -24
      Font.Name = 'Asap'
      Font.Style = [fsBold]
      ParentBackground = False
      ParentFont = False
      TabOrder = 0
      OnClick = exitBClick
      OnMouseEnter = exitBMouseEnter
      OnMouseLeave = exitBMouseLeave
    end
    object Dch: TCheckBox
      Left = 48
      Top = 73
      Width = 209
      Height = 17
      TabOrder = 1
      OnClick = DchClick
    end
    object Panel2: TPanel
      Left = 19
      Top = 104
      Width = 549
      Height = 191
      BevelOuter = bvNone
      TabOrder = 2
      object Label1: TLabel
        Left = 40
        Top = 45
        Width = 103
        Height = 22
        Caption = 'regular time'
      end
      object Label2: TLabel
        Left = 40
        Top = 73
        Width = 84
        Height = 22
        Caption = 'extra time'
      end
      object Label3: TLabel
        Left = 40
        Top = 106
        Width = 225
        Height = 22
        Caption = 'penalty-shootout               -'
      end
      object Label4: TLabel
        Left = 40
        Top = 139
        Width = 167
        Height = 22
        Caption = 'special circustances'
      end
      object t1L: TLabel
        Left = 150
        Top = 5
        Width = 40
        Height = 33
        Alignment = taRightJustify
        Caption = 't1L'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -29
        Font.Name = 'Asap'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object t2L: TLabel
        Left = 358
        Top = 5
        Width = 40
        Height = 33
        Caption = 't2L'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -29
        Font.Name = 'Asap'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object rb4: TRadioButton
        Left = 16
        Top = 141
        Width = 137
        Height = 17
        TabOrder = 0
        OnClick = rb1Click
      end
      object rb3: TRadioButton
        Left = 16
        Top = 109
        Width = 127
        Height = 17
        TabOrder = 1
        OnClick = rb1Click
      end
      object rb2: TRadioButton
        Left = 16
        Top = 76
        Width = 88
        Height = 17
        TabOrder = 2
        OnClick = rb1Click
      end
      object rb1: TRadioButton
        Left = 16
        Top = 48
        Width = 81
        Height = 17
        Checked = True
        TabOrder = 3
        TabStop = True
        OnClick = rb1Click
      end
      object Edit1: TEdit
        Left = 208
        Top = 0
        Width = 57
        Height = 44
        Alignment = taCenter
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -31
        Font.Name = 'Asap'
        Font.Style = [fsBold]
        NumbersOnly = True
        ParentFont = False
        TabOrder = 4
        Text = '0'
      end
      object Edit2: TEdit
        Left = 281
        Top = 0
        Width = 57
        Height = 44
        Alignment = taCenter
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -31
        Font.Name = 'Asap'
        Font.Style = [fsBold]
        NumbersOnly = True
        ParentFont = False
        TabOrder = 5
        Text = '0'
      end
      object Edit3: TEdit
        Left = 207
        Top = 101
        Width = 45
        Height = 32
        Alignment = taCenter
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -21
        Font.Name = 'Asap'
        Font.Style = []
        NumbersOnly = True
        ParentFont = False
        TabOrder = 6
        Text = '0'
      end
      object Edit4: TEdit
        Left = 272
        Top = 101
        Width = 45
        Height = 32
        Alignment = taCenter
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -21
        Font.Name = 'Asap'
        Font.Style = []
        NumbersOnly = True
        ParentFont = False
        TabOrder = 7
        Text = '0'
      end
      object Panel1: TPanel
        Left = 213
        Top = 139
        Width = 228
        Height = 46
        BevelOuter = bvNone
        TabOrder = 8
        object SCT1L: TLabel
          Left = 24
          Top = 0
          Width = 89
          Height = 18
          Caption = 'regular time'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -16
          Font.Name = 'Asap'
          Font.Style = []
          ParentFont = False
        end
        object SCT2L: TLabel
          Left = 24
          Top = 24
          Width = 73
          Height = 18
          Caption = 'extra time'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -16
          Font.Name = 'Asap'
          Font.Style = []
          ParentFont = False
        end
        object SCrb2: TRadioButton
          Left = 8
          Top = 24
          Width = 145
          Height = 17
          TabOrder = 0
        end
        object SCrb1: TRadioButton
          Left = 8
          Top = 1
          Width = 145
          Height = 17
          Checked = True
          TabOrder = 1
          TabStop = True
        end
      end
    end
  end
  object closeT: TTimer
    Enabled = False
    Interval = 650
    OnTimer = closeTTimer
    Left = 176
    Top = 24
  end
end
