﻿unit venues;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, jpeg, ExtCtrls, StdCtrls, pngimage, ShellAPI;

type
  TFVen = class(TForm)
    Panel9: TPanel;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    Panel6: TPanel;
    Panel7: TPanel;
    Panel8: TPanel;
    img: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    flagImg: TImage;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    exitB: TPanel;
    procedure Panel1MouseEnter(Sender: TObject);
    procedure Panel1MouseLeave(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Panel1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure exitBClick(Sender: TObject);
    procedure Label7Click(Sender: TObject);
    procedure Label8Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FVen: TFVen;
  lnk1, lnk2: string;

implementation

{$R *.dfm}

uses Euro12_pas, functii, DataTypes;

procedure TFVen.Panel1Click(Sender: TObject);
var x, i: byte;
begin
  for i:=1 to 8 do
    if Sender=TPanel(FindComponent('Panel'+inttostr(i))) then begin x:=i-1; TPanel(FindComponent('Panel'+inttostr(i))).Font.Style:=[fsBold] end
    else begin TPanel(FindComponent('Panel'+inttostr(i))).Font.Style:=TPanel(FindComponent('Panel'+inttostr(i))).Font.Style-[fsBold] end;
  Label1.Caption:=v[x].Name; Label3.Caption:=v[x].City;
  Label4.Caption:=TeamFullNameByID(v[x].Country);
  Label6.Caption:=inttostr(v[x].Capacity div 1000)+ThousandSeparator+formatfloat('000', v[x].Capacity mod 1000);
  flagImg.Picture.LoadFromFile('photos\'+v[x].Country+'.png');
  if fileexists('photos\'+v[x].City+'.jpg') then img.Picture.LoadFromFile('photos\'+v[x].City+'.jpg') else img.Picture.LoadFromFile('photos\UNK.png');
  lnk1:='http://www.google.ro/search?sourceid=chrome&ie=UTF-8&q='+web_str(v[x].Name+' '+v[x].City);
  lnk2:='http://www.google.ro/search?gcx=w&q=hello&um=1&ie=UTF-8&hl=en&tbm=isch&source=og&sa=N&tab=wi&biw=1280&bih=877#q='+web_str(v[x].Name+' '+v[x].City)+'&um=1&hl=en&tbm=isch&source=lnt&tbs=isz:m&sa=X&ei=Wy1ET-2fNMOg4gTVpNQ8&ved=0CAoQpwUoAQ&fp=1&biw=1364&bih=683&bav=on.2,or.r_gc.r_pw.r_cp.r_qf.,cf.osb&cad=b';
end;

procedure TFVen.Panel1MouseEnter(Sender: TObject);
begin
  if Sender is TLabel then TLabel(Sender).Font.Color:=clYellow
  else if Sender is TPanel then TPanel(Sender).Font.Color:=clYellow
end;

procedure TFVen.Panel1MouseLeave(Sender: TObject);
begin
  if Sender is TLabel then TLabel(Sender).Font.Color:=clWhite
  else if Sender is TPanel then TPanel(Sender).Font.Color:=clWhite
end;

procedure TFVen.exitBClick(Sender: TObject);
begin
  FVen.Close
end;

procedure TFVen.FormCreate(Sender: TObject);
begin
  Img.Picture.RegisterFileFormat('.PNG', 'port netw graph', TPngImage);
  flagImg.Picture.RegisterFileFormat('.PNG', 'port netw graph', TPngImage)
end;

procedure TFVen.FormShow(Sender: TObject);
var i: word;
begin
  for i:=0 to nv-1 do TPanel(FindComponent('Panel'+inttostr(i+1))).Caption:=v[i].City;
  Panel1Click(Panel1)
end;

procedure TFVen.Label7Click(Sender: TObject);
begin ShellExecute(Handle, 'open', PChar(lnk1), nil, nil, SW_SHOW) end;

procedure TFVen.Label8Click(Sender: TObject);
begin ShellExecute(Handle, 'open', PChar(lnk2), nil, nil, SW_SHOW) end;

end.
