unit Euro12_pas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, JPEG, pngimage, MPlayer, ID3v1Library,
  ID3v2Library, MMSystem, ImgList, Menus, ShellAPI;

const MCI_SETAUDIO = $0873; MCI_DGV_SETAUDIO_VOLUME = $4002; MCI_DGV_SETAUDIO_ITEM = $00800000; MCI_DGV_SETAUDIO_VALUE = $01000000; MCI_DGV_STATUS_VOLUME = $4019;
      ESNM: array[0..2] of byte=(4, 2, 1);

type MCI_DGV_SETAUDIO_PARMS = record
  dwCallback, dwItem, dwValue, dwOver: DWORD;
  lpstrAlgorithm, lpstrQuality: PChar;
end;

type MCI_STATUS_PARMS = record
  dwCallback, dwReturn, dwItem, dwTrack: DWORD;
end;

type
  TF1 = class(TForm)
    logL: TLabel;
    Image1: TImage;
    testLB: TListBox;
    Image2: TImage;
    SectionTitleL: TLabel;
    NormalL: TLabel;
    GroupStagePanel: TPanel;
    FinalStagePanel: TPanel;
    refB: TButton;
    showProgEdB: TButton;
    pB1: TPanel;
    pB2: TPanel;
    exitB: TPanel;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Image3: TImage;
    mpStatusL: TLabel;
    mpSongTagL: TLabel;
    mplT: TTimer;
    startMPT: TTimer;
    mpl: TMediaPlayer;
    optMI: TImage;
    optM: TPopupMenu;
    ImageList1: TImageList;
    Abouttheprogram1: TMenuItem;
    Moreactions1: TMenuItem;
    Resetallmatches1: TMenuItem;
    Fillwithrandomresults1: TMenuItem;
    nAMDL: TLabel;
    Open1: TMenuItem;
    Knownbugs1: TMenuItem;
    Logfile1: TMenuItem;
    Datafolder1: TMenuItem;
    Photosfolder1: TMenuItem;
    Musicfolder1: TMenuItem;
    Utilitiesfolder1: TMenuItem;
    Exit1: TMenuItem;
    mInfoL: TLabel;
    Playlistinfo1: TMenuItem;
    Play1: TMenuItem;
    Matchtimeutility1: TMenuItem;
    procedure LabelMouseEnter1(Sender: TObject);
    procedure LabelMouseLeave1(Sender: TObject);
    procedure GroupLabelMouseEnter(Sender: TObject);
    procedure GroupLabelMouseLeave(Sender: TObject);
    procedure MatchLabelClick(Sender: TObject);
    procedure MatchLabelDoubleClick(Sender: TObject);
    procedure MatchLabelMouseEnter(Sender: TObject);
    procedure MatchLabelMouseLeave(Sender: TObject);
    procedure ESMatchLabelMouseEnter(Sender: TObject);
    procedure ESMatchLabelMouseLeave(Sender: TObject);
    procedure GroupPosLabelMouseEnter(Sender: TObject);
    procedure GroupPosLabelMouseLeave(Sender: TObject);
    procedure TeamLabelClick(Sender: TObject);
    procedure MPPanelResize(Sender: TObject);
    procedure MP2PanelResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure exitBClick(Sender: TObject);
    procedure refBClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure showProgEdBClick(Sender: TObject);
    procedure pB1Click(Sender: TObject);
    procedure Panel2Click(Sender: TObject);
    procedure Panel3Click(Sender: TObject);
    procedure mplTTimer(Sender: TObject);
    procedure startMPTTimer(Sender: TObject);
    procedure mpStatusLClick(Sender: TObject);
    procedure mpSongTagLClick(Sender: TObject);
    procedure Panel1Click(Sender: TObject);
    procedure optMIClick(Sender: TObject);
    procedure Resetallmatches1Click(Sender: TObject);
    procedure Fillwithrandomresults1Click(Sender: TObject);
    procedure FinalStagePanelResize(Sender: TObject);
    procedure Abouttheprogram1Click(Sender: TObject);
    procedure Knownbugs1Click(Sender: TObject);
    procedure Logfile1Click(Sender: TObject);
    procedure Datafolder1Click(Sender: TObject);
    procedure Photosfolder1Click(Sender: TObject);
    procedure Musicfolder1Click(Sender: TObject);
    procedure Utilitiesfolder1Click(Sender: TObject);
    procedure Playlistinfo1Click(Sender: TObject);
    procedure PlayTrackClick(Sender: TObject);
    procedure Matchtimeutility1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

type TMatchesPanelGroup=record
  p: TPanel;
  grTitL: TLabel;
  l: array[0..5, 0..2] of TLabel;
  c: array[0..3, 0..5] of TLabel;
  il: array[0..5, 0..1] of TImage;
  ic: array[0..3] of TImage;
end;

var
  F1: TF1;
  mp: array[0..3] of TMatchesPanelGroup;
  mp2: array[0..2] of TMatchesPanelGroup;
  selMG, selMM, selGG, selGM, selG, selESP, selESM: integer;

  SongList: TStringList;
  mptag: TID3v1Tag;
  iMPTag, currSong: integer;

implementation

{$R *.dfm}

uses functii, DataTypes, progEditor, venues, matches, teams, MatchTime;

procedure TF1.Logfile1Click(Sender: TObject);
begin ShellExecute(Handle, 'open', PChar('data\log.txt'), nil, nil, SW_SHOW) end;

procedure TF1.Datafolder1Click(Sender: TObject);
begin ShellExecute(Handle, 'open', PChar('data'), nil, nil, SW_SHOW) end;

procedure TF1.Photosfolder1Click(Sender: TObject);
begin ShellExecute(Handle, 'open', PChar('photos'), nil, nil, SW_SHOW) end;

procedure TF1.Musicfolder1Click(Sender: TObject);
begin ShellExecute(Handle, 'open', PChar('music'), nil, nil, SW_SHOW) end;

procedure TF1.Utilitiesfolder1Click(Sender: TObject);
begin ShellExecute(Handle, 'open', PChar('utilities'), nil, nil, SW_SHOW) end;

procedure MPSetVolume(MP: TMediaPlayer; Volume: Integer);
{Set Volume, range 0 - 1000}
var p: MCI_DGV_SETAUDIO_PARMS;
begin
  log('Setting volume to '+floattostr(Volume/10)+'%...');
  p.dwCallback := 0;
  p.dwItem := MCI_DGV_SETAUDIO_VOLUME;
  p.dwValue := Volume;
  p.dwOver := 0;
  p.lpstrAlgorithm := nil;
  p.lpstrQuality := nil;
  mciSendCommand(MP.DeviceID, MCI_SETAUDIO, MCI_DGV_SETAUDIO_VALUE or MCI_DGV_SETAUDIO_ITEM, Cardinal(@p))
end;

function MPGetVolume(MP: TMediaPlayer): Integer;
{Get Volume, range 0 - 1000}
var p: MCI_STATUS_PARMS;
begin
  p.dwCallback := 0;
  p.dwItem := MCI_DGV_STATUS_VOLUME;
  mciSendCommand(MP.DeviceID, MCI_STATUS, MCI_STATUS_ITEM, Cardinal(@p)) ;
  Result := p.dwReturn
end;

procedure TF1.PlayTrackClick(Sender: TObject);
var i: word;
begin
  for i:=0 to Play1.Count-1 do if Sender=Play1.Items[i] then break; log('Trying to play "'+SongList[i]+'"...');
  mpl.FileName:=getcurrentdir+'\music\'+SongList[i]; mpl.Open; mpl.Play; MPSetVolume(mpl, 200)
end;

procedure TF1.Playlistinfo1Click(Sender: TObject);
var s, fn0: string; i: word; p, tl: longint; x: TID3v1Tag; mpx: TMediaPlayer;
begin
  try
  if startMPT.Enabled then exit;
  if not mpStatusL.Enabled then begin showmessage('The playlist was not created because you cancelled the initialization. Restart the program to initialize.'); exit end;
  s:='Track list:'+nl; tl:=0; x:=TID3v1Tag.Create; fn0:=mpl.FileName; p:=mpl.Position; mpl.Close;
  mpx:=TMediaPlayer.Create(Self); mpx.TimeFormat:=tfMilliseconds; mpx.Parent:=Self; mpx.Visible:=false;
  for i:=0 to SongList.Count-1 do
    begin
      try
      log('Loading file for playlist info: "'+getcurrentdir+'\music\'+SongList[i]+'"...');
      x.LoadFromFile(getcurrentdir+'\music\'+SongList[i]); mpx.Close; mpx.FileName:=getcurrentdir+'\music\'+SongList[i]; mpx.Open; inc(tl, mpx.Length);
      s:=s+nl+inttostr(i+1)+'. Song "'+x.Title+'" (length '+song_length(mpx.Length)+') by '''+x.Artist+''', from the album "'+x.Album+'" (launched '+x.Year+')';
      except on E:Exception do showmessage('Playlistinfo1Click (iteration) ERROR (i='+inttostr(i+1)+'/'+inttostr(SongList.Count)+'): '+E.ClassName+': '+E.Message) end;
    end;
  mpl.FileName:=fn0; F1.mpl.Open; mpl.Position:=p; mpl.Play; MPSetVolume(mpl, 200);
  clean_str(s, true, true); showmessage(s+dnl+'Total length: '+song_length(tl))
  except on E:Exception do showmessage('Playlistinfo1Click (somewhere below) ERROR (i='+inttostr(i+1)+'/'+inttostr(SongList.Count)+'): '+E.ClassName+': '+E.Message) end;
end;

procedure TF1.exitBClick(Sender: TObject);
begin
  if mpl.Mode=mpPlaying then mpl.Stop;
  Halt
end;

procedure TF1.FormShow(Sender: TObject);
begin
  pB1Click(pB1)
end;

procedure TF1.GroupLabelMouseEnter(Sender: TObject);
var i, j: word; d1, d2: TDate; xs: string;
begin
  TLabel(Sender).Font.Color:=clYellow; selG:=100;
  for i:=0 to 3 do  if (Sender=mp[i].grTitL) or (Sender=mp2[i].grTitL) then selG:=i;
  for i:=0 to 5 do for j:=0 to 2 do mp[selG].l[i,j].Font.Color:=clYellow;
  for i:=0 to 3 do for j:=0 to 5 do mp[selG].c[i,j].Font.Color:=clYellow;
  //
  d1:=encodedate(2050, 1, 1); d2:=encodedate(1960, 1, 1);
  if GroupStagePanel.Visible then
    begin
      for i:=0 to 23 do if m[i].Stage='group'+chr(65+selG) then
        begin if m[i].When<d1 then d1:=m[i].When; if m[i].When>d2 then d2:=m[i].When end
    end
  else if FinalStagePanel.Visible then
    begin
      for j:=0 to ESNM[selG]-1 do
        begin i:=ESMatchIDbyLabelIJ(selG, j); if m[i].When<d1 then d1:=m[i].When; if m[i].When>d2 then d2:=m[i].When end
    end;
  if d1<>d2 then xs:=formatdatetime('" (matches "d mmm yy" - "', d1)+formatdatetime('d mmm yy)', d2) else xs:=formatdatetime(' (d mmm yy)', d1);
  mInfoL.Caption:=TLabel(Sender).Caption+xs;
end;

procedure TF1.GroupLabelMouseLeave(Sender: TObject);
var i, j: word;
begin
  TLabel(Sender).Font.Color:=clWhite; mInfoL.Caption:='';
  for i:=0 to 5 do for j:=0 to 2 do mp[selG].l[i,j].Font.Color:=clWhite;
  for i:=0 to 3 do for j:=0 to 5 do mp[selG].c[i,j].Font.Color:=clWhite
end;

procedure TF1.Knownbugs1Click(Sender: TObject);
var s: string;
begin
  s:='Kindafabug: When modifying a group match causing the two qualifying teams to switch places, the eliminatory match results remain the same; should they? or should they become undisputed and cause this effect all the way to the final?';
  s:=s+dnl+'Bug: When cancelling a group match, causing an incomplete number of group matches, the previosuly established and played quarter-finals appear undisputed, as desired, but are saved as disputed.';
  s:=s+dnl+'Problem: unknown error on Playlist info click';
  showmessage(s)
end;

procedure TF1.optMIClick(Sender: TObject);
begin optM.Popup(optMI.Left, optMI.Top) end;

procedure TF1.MatchLabelClick(Sender: TObject);
var i, j, x: word; ok: boolean;
begin
  ok:=false;
  for i:=0 to 2 do for j:=0 to ESNM[i]-1 do
    if Sender=mp2[i].l[j,1] then
      begin x:=ESMatchIDbyLabelIJ(i, j); ok:=true; break end;
  if not ok then
    for i:=0 to 3 do for j:=0 to 5 do
      if Sender=mp[i].l[j,1] then
        begin x:=GSMatchIDbyLabelIJ(i,j); break end;
  FM.Show; FM.mlb.ItemIndex:=x; FM.mlbChange(Sender)
end;

procedure TF1.MatchLabelDoubleClick(Sender: TObject);
begin if FM.Showing then FM.editLClick(Sender) end;

procedure TF1.MatchLabelMouseEnter(Sender: TObject);
var i, j, k: word;
begin
  for k:=0 to 2 do mp[selMG].l[selMM,k].Font.Color:=clWhite;
  for i:=0 to 3 do for j:=0 to 5 do for k:=0 to 2 do if Sender=mp[i].l[j,k] then
    begin selMG:=i; selMM:=j; mInfoL.Caption:='Match '+inttostr(GSMatchIDbyLabelIJ(i,j)+1)+': '+mp[i].l[j,0].Caption+'-'+mp[i].l[j,2].Caption+formatdatetime(' (ddd, d mmm yyyy, h:nn)', m[GSMatchIDbyLabelIJ(i,j)].When); break end;
  for k:=0 to 2 do mp[selMG].l[selMM,k].Font.Color:=clYellow
end;

procedure TF1.MatchLabelMouseLeave(Sender: TObject);
var k: word;
begin
  mInfoL.Caption:='';
  for k:=0 to 2 do mp[selMG].l[selMM,k].Font.Color:=clWhite
end;

procedure TF1.Matchtimeutility1Click(Sender: TObject);
begin if FMT.Showing then FMT.OnShow(Matchtimeutility1) else FMT.Show end;

procedure TF1.Abouttheprogram1Click(Sender: TObject);
begin showmessage('by BogdyBBA'+dnl+'v1.0 alpha - May 6th to 10th, 12th, 20th, 25th, 30th, 2012'+nl+'v1.0 beta - June 4th, 7th, 8th, 9th, 2012') end;

procedure TF1.ESMatchLabelMouseEnter(Sender: TObject);
var i, j, k: word;
begin
  for i:=0 to 2 do for j:=0 to ESNM[i]-1 do for k:=0 to 2 do if Sender=mp2[i].l[j,k] then
    begin selESP:=i; selESM:=j; mInfoL.Caption:='Match '+inttostr(ESMatchIDbyLabelIJ(i, j)+1)+': '+mp2[i].l[j,0].Caption+'-'+mp2[i].l[j,2].Caption+formatdatetime(' (ddd, d mmm yyyy, h:nn)', m[ESMatchIDbyLabelIJ(i,j)].When); break end;
  for k:=0 to 2 do mp2[selESP].l[selESM,k].Font.Color:=clYellow
end;

procedure TF1.ESMatchLabelMouseLeave(Sender: TObject);
var k: word;
begin
  mInfoL.Caption:='';
  for k:=0 to 2 do mp2[selESP].l[selESM,k].Font.Color:=clWhite
end;

procedure TF1.TeamLabelClick(Sender: TObject);
var x: word;
begin
  FT.Show;
  for x:=1 to 16 do
  if TPanel(FT.FindComponent('Panel'+inttostr(x))).Caption=TLabel(Sender).Caption then
    begin FT.Panel1Click(TPanel(FT.FindComponent('Panel'+inttostr(x)))); break end
end;

procedure TF1.startMPTTimer(Sender: TObject);
var i: word; s: string; x: TMenuItem; t: TID3v1Tag;
begin
  try
  startMPT.Enabled:=false; iMPTag:=0; SongList:=TStringList.Create;
  scan_songs(getcurrentdir+'\music\', SongList); if SongList.Count=0 then begin mpStatusL.Enabled:=false; mpStatusL.Caption:='There are no songs in the "\music" folder to play.'; mpSongTagL.Caption:='---'; exit end;
  mptag:=TID3v1Tag.Create; mpSongTagLClick(startMPT); mpSongTagL.Enabled:=true;
  //
  t:=TID3v1Tag.Create;
  for i:=0 to SongList.Count-1 do
    begin
      x:=TMenuItem.Create(Self); t.LoadFromFile(getcurrentdir+'\music\'+SongList[i]);
      s:=inttostr(i+1)+'. '+t.Artist+' - '+t.Title; clean_str(s, true, true); x.Caption:=s;
      x.ImageIndex:=587; x.OnClick:=PlayTrackClick; Play1.Insert(Play1.Count, x);
    end;
  except on E:Exception do showmessage('Failed to initialize and start music player.'+dnl+E.ClassName+': '+E.Message) end;
end;

procedure TF1.mplTTimer(Sender: TObject);
var xs, stS: string;
begin
  try
  if mpl.Mode=mpStopped then mpl.Play;
  if mpl.Mode=mpPlaying then stS:='Playing' else stS:='Paused';
  mpStatusL.Caption:=stS+' ('+song_length(mpl.Position)+' / '+song_length(mpl.Length)+')';
  inc(iMPTag); if iMPTag>19 then iMPTag:=1;
  case iMPTag div 5 of
  0, 1: xs:=stringreplace(mptag.Artist+' - '+mptag.Title, '&', '&&', [rfReplaceAll]);
  2: xs:=stringreplace('Album: '+mptag.Album+' ('+mptag.Year+')', '&', '&&', [rfReplaceAll]);
  3: xs:='Song length: '+stringreplace(song_length(mpl.Length), ':', ' minute(s) and ', [rfReplaceAll])+' second(s)';
  end;
  clean_str(xs, true, true); mpSongTagL.Caption:=xs
  except on E:Exception do log('mplTTimer ERROR: '+E.ClassName+': '+E.Message) end;
end;

procedure TF1.GroupPosLabelMouseEnter(Sender: TObject);
var i, j, k: word;
begin
  for k:=0 to 5 do mp[selGG].c[selGM,k].Font.Color:=clWhite;
  for i:=0 to 3 do for j:=0 to 3 do for k:=0 to 5 do if Sender=mp[i].c[j,k] then begin selGG:=i; selGM:=j; break end;
  for i:=0 to ne-1 do if t[i].Name=mp[selGG].c[selGM,1].Caption then break; if t[i].Nickname<>'none/unknown' then mInfoL.Caption:='"'+t[i].Nickname+'"';
  for k:=0 to 5 do mp[selGG].c[selGM,k].Font.Color:=clYellow
end;

procedure TF1.GroupPosLabelMouseLeave(Sender: TObject);
var k: word;
begin
  mInfoL.Caption:='';
  for k:=0 to 5 do mp[selGG].c[selGM,k].Font.Color:=clWhite
end;

procedure TF1.LabelMouseEnter1(Sender: TObject);
begin
  if Sender is TLabel then TLabel(Sender).Font.Color:=$0001EBFE
  else if Sender is TPanel then TPanel(Sender).Font.Color:=$0001EBFE
end;

procedure TF1.LabelMouseLeave1(Sender: TObject);
begin
  if Sender is TLabel then TLabel(Sender).Font.Color:=clWhite
  else if Sender is TPanel then TPanel(Sender).Font.Color:=clWhite
end;

procedure TF1.Fillwithrandomresults1Click(Sender: TObject);
var i: word;
begin
if MessageDlg('This will set random results for matches that have not been disputed. Are you sure you want to do this?'+dnl+'PS: The results will not be saved. If you want to save them, edit a match and click ''Save''. If you want to cancel the random results, restart the program (without saving anything).', mtConfirmation, [mbYes, mbCancel], 0)=mrYes then
    begin
      for i:=0 to nm-1 do if not ((m[i].Disputed) or (m[i].Team1='UNK') or (m[i].Team2='UNK')) then
        begin m[i].Disputed:=true; m[i].Goals1:=random(6); m[i].Goals2:=random(6); if i>23 then while (m[i].Goals1=m[i].Goals2) do begin m[i].Goals1:=random(6); m[i].Goals2:=random(6) end end;
      F1.refBClick(Fillwithrandomresults1)
    end
end;

procedure TF1.FinalStagePanelResize(Sender: TObject);
begin nAMDL.Top:=FinalStagePanel.Height div 2; nAMDL.Left:=FinalStagePanel.Width div 2 - nAMDL.Width div 2 end;

procedure TF1.FormCreate(Sender: TObject);
  procedure SetLabelAttributes(var x: TLabel; typ: byte);
  begin
    case typ of
    1: begin with x.Font do begin Size:=16; {Color:=$00FFD2FF;} Style:=[fsBold]; end end;
    2: begin x.AutoSize:=false; x.Alignment:=taCenter; with x.Font do begin Style:=[fsBold]; end end;
    3: begin x.AutoSize:=false; {with x.Font do begin Size:=16; Color:=$00FFD2FF; Style:=[fsBold]; end} end;
    4: begin x.AutoSize:=false; x.Alignment:=taCenter; {with x.Font do begin Style:=[fsBold]; end} end;
    5: begin x.AutoSize:=false; x.Alignment:=taCenter; with x.Font do begin Style:=[fsBold]; end end;
    end;
  end;
var i, j, k: word;
begin
  //checking
  if (Screen.Fonts.IndexOf('Asap')=-1) or (Screen.Fonts.IndexOf('Ubuntu')=-1) then begin showmessage('You are missing one or more fonts that are used in the program (Asap, Ubuntu). The program will now close; go to "\utilities\fonts" and install all of them, then restart.'); Halt end;
  //init
  selMM:=0; selMG:=0; selGM:=0; selGG:=0;
  mpl.TimeFormat:=tfMilliseconds;
  mpStatusL.Caption:='Initializing music player...'; mpSongTagL.Caption:='Extracting song information...';
  //creating elements
  try
  for i:=0 to 3 do
    with mp[i] do
      begin
        p:=TPanel.Create(Self); p.Parent:=GroupStagePanel; p.BevelOuter:=bvNone; p.OnResize:=MPPanelResize;
        grTitL:=TLabel.Create(Self); grTitL.Parent:=p; SetLabelAttributes(grTitL, 1); grTitL.Caption:='Group '+chr(65+i);
        grTitL.OnMouseEnter:=GroupLabelMouseEnter; grTitL.OnMouseLeave:=GroupLabelMouseLeave;
        for j:=0 to 5 do
          begin
            for k:=0 to 1 do begin il[j,k]:=TImage.Create(Self); il[j,k].Parent:=p; il[j,k].autosize:=true end;
            for k:=0 to 2 do
              begin
                l[j,k]:=TLabel.Create(Self); l[j,k].Parent:=p; SetLabelAttributes(l[j,k], 3-integer(k=1));
                  if k=0 then l[j,k].Alignment:=taRightJustify;
                l[j,k].Caption:='l['+inttostr(j)+','+inttostr(k)+']';
                l[j,k].OnMouseEnter:=MatchLabelMouseEnter; l[j,k].OnMouseLeave:=MatchLabelMouseLeave;
              end;
            l[j,1].Cursor:=crHandPoint; l[j,1].OnClick:=MatchLabelClick; l[j,1].OnDblClick:=MatchLabelDoubleClick
          end;
        for j:=0 to 3 do
          begin
            ic[j]:=TImage.Create(Self); ic[j].Parent:=p; ic[j].autosize:=true;
            for k:=0 to 5 do
              begin
                c[j,k]:=TLabel.Create(Self); c[j,k].Parent:=p; SetLabelAttributes(c[j,k], 4+integer(k in [1, 5]));
                if (k in [0,5]) then c[j,k].Alignment:=taRightJustify else if k=1 then c[j,k].Alignment:=taLeftJustify;
                c[j,k].Caption:='c['+inttostr(j)+','+inttostr(k)+']';
                c[j,k].OnMouseEnter:=GroupPosLabelMouseEnter; c[j,k].OnMouseLeave:=GroupPosLabelMouseLeave
              end;
            c[j,1].Cursor:=crHandPoint; c[j,1].OnClick:=TeamLabelClick
          end;
      end;
  except on E:Exception do showmessage('FormCreate element creation ERROR; i='+inttostr(i)+', j='+inttostr(j)+', k='+inttostr(k)+dnl+E.ClassName+': '+E.Message) end;try
  //
  for i:=0 to 2 do
    with mp2[i] do
      begin
        p:=TPanel.Create(Self); p.Parent:=FinalStagePanel; p.BevelOuter:=bvNone; p.OnResize:=MP2PanelResize;
        grTitL:=TLabel.Create(Self); grTitL.Parent:=p; SetLabelAttributes(grTitL, 1);
        case i of 0: grTitL.Caption:='Quarter-finals'; 1: grTitL.Caption:='Semi-finals'; 2: grTitL.Caption:='Final' end;
        grTitL.OnMouseEnter:=GroupLabelMouseEnter; grTitL.OnMouseLeave:=GroupLabelMouseLeave;
        for j:=0 to ESNM[i]-1 do
          begin
            for k:=0 to 1 do begin il[j,k]:=TImage.Create(Self); il[j,k].Parent:=p; il[j,k].autosize:=true end;
            for k:=0 to 2 do
              begin
                l[j,k]:=TLabel.Create(Self); l[j,k].Parent:=p; SetLabelAttributes(l[j,k], 3-integer(k=1));
                  if k=0 then l[j,k].Alignment:=taRightJustify;
                l[j,k].Caption:='l['+inttostr(j)+','+inttostr(k)+']';
                l[j,k].OnMouseEnter:=ESMatchLabelMouseEnter; l[j,k].OnMouseLeave:=ESMatchLabelMouseLeave;
              end;
            l[j,1].Cursor:=crHandPoint; l[j,1].OnClick:=MatchLabelClick; l[j,1].OnDblClick:=MatchLabelDoubleClick
          end;
      end;
  except on E:Exception do showmessage('FormCreate element creation ERROR; i='+inttostr(i)+', j='+inttostr(j)+', k='+inttostr(k)+dnl+E.ClassName+': '+E.Message) end;
  //finalize
  try
  if Screen.Width<=1366 then Image1.Picture.LoadFromFile('utilities\bgimg1.jpg') else Image1.Picture.LoadFromFile('utilities\bgimg0.jpg');
  ReadData;
  except on E:Exception do showmessage('FormCreate final section ERROR;'+dnl+E.ClassName+': '+E.Message) end;
end;

procedure TF1.MPPanelResize(Sender: TObject);
var x, w, w2, {h,} i, j: word;
begin
  try
  for x:=0 to 3 do if Sender = mp[x].p then break; w:=mp[x].p.Width; w2:=w div 2; //h:=mp[x].p.Height;
  with mp[x] do
    begin
      grTitL.Top:=10; grTitL.Left:=w div 2-grTitL.Width div 2;
      for i:=0 to 5 do
        begin
          l[i,0].Left:=0; l[i,1].Left:=w2-25; l[i,2].Left:=w2+55;
          l[i,0].Width:=w2-55; l[i,1].Width:=50; l[i,2].Width:=l[i,0].Width;
          for j:=0 to 2 do
            l[i,j].Top:=(grTitL.Top+grTitL.Height+30) + i*(22+8);
          il[i,0].Left:=l[i,1].Left-25; il[i,1].Left:=l[i,1].Left+53;
          for j:=0 to 1 do il[i,j].Top:=l[i,0].Top+3;
        end;
      //
      for i:=0 to 3 do
        begin
          c[i,0].Left:=3; c[i,0].Width:=17;
          c[i,1].Left:=48; c[i,1].Width:=w2-c[i,1].Left-10;
          c[i,2].Left:=w2-10; c[i,2].Width:=50;
          c[i,3].Left:=w2+43; c[i,3].Width:=40;
          c[i,4].Left:=w2+86; c[i,4].Width:=35;
          c[i,5].Left:=w2+124; c[i,5].Width:=30;
          for j:=0 to 5 do
            c[i,j].Top:=(l[5,0].Top+l[5,0].Height+35) + i*(22+8);
          ic[i].Left:=22; ic[i].Top:=c[i,0].Top+3;
        end
    end
  except on E:Exception do showmessage('MPPanelResize ERROR; x='+inttostr(x)+', i='+inttostr(i)+', j='+inttostr(j)+dnl+E.ClassName+': '+E.Message) end
end;

procedure TF1.MP2PanelResize(Sender: TObject);
var x, w, w2, h0, hx, i, j: word;
begin
  try
  for x:=0 to 2 do if Sender = mp2[x].p then break; w:=mp2[x].p.Width; w2:=w div 2; h0:=mp2[x].grTitL.Height+25; hx:=mp2[x].p.Height-h0;
  with mp2[x] do
    begin
      grTitL.Top:=25; grTitL.Left:=w2-grTitL.Width div 2;
      for i:=0 to ESNM[x]-1 do
        begin
          l[i,0].Left:=0; l[i,1].Left:=w2-35; l[i,2].Left:=w2+45;
          l[i,0].Width:=w2-68; l[i,1].Width:=50; l[i,2].Width:=w2-40;
          for j:=0 to 2 do
            l[i,j].Top:=h0 + (i+1)*(hx div (ESNM[x]+1));
          il[i,0].Left:=l[i,1].Left-25; il[i,1].Left:=l[i,1].Left+53;
          for j:=0 to 1 do il[i,j].Top:=l[i,0].Top+3;
        end
    end
  except on E:Exception do showmessage('MP2PanelResize ERROR; x='+inttostr(x)+', i='+inttostr(i)+', j='+inttostr(j)+dnl+E.ClassName+': '+E.Message) end
end;

procedure TF1.mpSongTagLClick(Sender: TObject);
begin
  if Sender=startMPT then begin if SongList.IndexOf('Young Empires - The Earth plates are shifting.mp3')>=0 then currSong:=SongList.IndexOf('Young Empires - The Earth plates are shifting.mp3') end
    else begin if SongList.Count>1 then begin repeat currSong:=random(SongList.Count) until SongList[currSong]<>mpl.FileName end else currSong:=0 end;
  mptag.Clear; mptag.LoadFromFile(getcurrentdir+'\music\'+SongList[currSong]);
  mpl.FileName:=getcurrentdir+'\music\'+SongList[currSong]; mpl.Open; mpl.Play; MPSetVolume(mpl, 200);
  iMPTag:=0; mplT.Enabled:=true
end;

procedure TF1.mpStatusLClick(Sender: TObject);
begin if startMPT.Enabled then begin startMPT.Enabled:=false; mpStatusL.Enabled:=false; exit end; if mpl.Mode=mpPlaying then mpl.Pause else mpl.Play end;

procedure TF1.Panel1Click(Sender: TObject);
begin if FT.Showing then FT.Close else FT.Show end;

procedure TF1.Panel2Click(Sender: TObject);
begin if FVen.Showing then FVen.Close else FVen.Show end;

procedure TF1.Panel3Click(Sender: TObject);
begin if FM.Showing then FM.Close else FM.Show end;

procedure TF1.pB1Click(Sender: TObject);
begin
  if Sender=pB1 then
    begin
      pB1.Font.Style:=[fsBold]; pB2.Font.Style:=pB2.Font.Style-[fsBold];
      pB1.Cursor:=crDefault; pB2.Cursor:=crHandPoint;
      GroupStagePanel.Visible:=true; FinalStagePanel.Visible:=false
    end
  else
    begin
      pB2.Font.Style:=[fsBold]; pB1.Font.Style:=pB1.Font.Style-[fsBold];
      pB2.Cursor:=crDefault; pB1.Cursor:=crHandPoint;
      GroupStagePanel.Visible:=false; FinalStagePanel.Visible:=true
    end;
  refBClick(Sender)
end;

procedure TF1.FormResize(Sender: TObject);
var w, h, i, x: word;
begin
  try
  w:=F1.Width; h:=F1.Height;
  Image1.Left:=0; Image1.Top:=0; Image1.Width:=w; Image1.Height:=h;
  exitB.Left:=w-exitB.Width-5; exitB.Top:=h-exitB.Height-5; logL.Left:=exitB.Left-logL.Width-5; logL.Top:=h-logL.Height-7;
  pB1.Left:=Image2.Left+Image2.Width+6; pB1.Width:=(w-Image2.Width-18) div 2; pB2.Left:=pB1.Left+pB1.Width+6; pB2.Width:=pB1.Width;
  //
  x:=(w-Image2.Width-30) div 4;
  for i:=1 to 4 do begin TPanel(FindComponent('Panel'+inttostr(i))).Left:=Image2.Left+Image2.Width+(i-1)*(x+6)+6; TPanel(FindComponent('Panel'+inttostr(i))).Width:=x end;
  //
  GroupStagePanel.Left:=0; GroupStagePanel.Width:=w; GroupStagePanel.Top:=Image2.Top+Image2.Height+5; GroupStagePanel.Height:=exitB.Top-Image2.Height-10;
  FinalStagePanel.Left:=GroupStagePanel.Left; FinalStagePanel.Top:=GroupStagePanel.Top; FinalStagePanel.Height:=GroupStagePanel.Height; FinalStagePanel.Width:=GroupStagePanel.Width;
  //
  for i:=0 to 3 do
    begin if not assigned(mp[i].p) then exit; mp[i].p.Left:=i*w div 4; mp[i].p.Width:=w div 4; mp[i].p.Top:=0; mp[i].p.Height:=GroupStagePanel.Height end;
  for i:=0 to 2 do
    begin if not assigned(mp2[i].p) then exit; mp2[i].p.Left:=i*w div 3; mp2[i].p.Width:=w div 3; mp2[i].p.Top:=0; mp2[i].p.Height:=FinalStagePanel.Height end
  except on E:Exception do showmessage('FormResize ERROR; i='+inttostr(i)+dnl+E.ClassName+': '+E.Message) end
end;

procedure TF1.refBClick(Sender: TObject);
var x, i, j, k: longint; allGroupMatchesDisputed: boolean;
begin
  mInfoL.Caption:='';
  if GroupStagePanel.Visible then
  begin
  for i:=0 to 3 do
    for j:=0 to 5 do
      begin
        x:=GSMatchIDbyLabelIJ(i,j);
        mp[i].l[j,0].Caption:=TeamFullNameByID(m[x].Team1); mp[i].l[j,2].Caption:=TeamFullNameByID(m[x].Team2);
        if m[x].Disputed then mp[i].l[j,1].Caption:=inttostr(m[x].Goals1)+'-'+inttostr(m[x].Goals2) else mp[i].l[j,1].Caption:='-';
        mp[i].il[j,0].Picture.LoadFromFile('photos\'+m[x].Team1+'.png'); mp[i].il[j,1].Picture.LoadFromFile('photos\'+m[x].Team2+'.png');
        for k:=0 to 2 do mp[i].l[j,k].Font.Color:=clWhite
      end;
  RecalculateGroups;
  for i:=0 to 3 do
    for j:=0 to 3 do
      with mp[i] do
        begin
          c[j,0].Caption:=inttostr(j+1)+'.';
          c[j,1].Caption:=TeamFullNameByID(g[i].Poz[j].TeamID);
          c[j,2].Caption:=inttostr(g[i].Poz[j].nv)+' '+inttostr(g[i].Poz[j].ne)+' '+inttostr(g[i].Poz[j].nd);
          c[j,3].Caption:=inttostr(g[i].Poz[j].gf)+'-'+inttostr(g[i].Poz[j].ga);
          c[j,4].Caption:=golaveraj(g[i].Poz[j].gol);
          c[j,5].Caption:=inttostr(g[i].Poz[j].pts);
          ic[j].Picture.LoadFromFile('photos\'+g[i].Poz[j].TeamID+'.png');
          for k:=0 to 5 do mp[i].c[j,k].Font.Color:=clWhite
        end
  end
  else if FinalStagePanel.Visible then
  begin
    RecalculateGroups;
    ProcessEliminatoryStage;
    //
    allGroupMatchesDisputed:=true; for i:=0 to 23 do if not m[i].Disputed then begin allGroupMatchesDisputed:=false; break end; nAMDL.Visible:=not allGroupMatchesDisputed; FinalStagePanel.Refresh;
    //
    for i:=0 to 2 do
      for j:=0 to ESNM[i]-1 do
        begin
          x:=ESMatchIDbyLabelIJ(i, j);
          if m[x].Disputed then mp2[i].l[j,1].Caption:=inttostr(m[x].Goals1)+'-'+inttostr(m[x].Goals2) else mp2[i].l[j,1].Caption:='-';

          if TeamFullNameByID(m[x].Team1)<>'unknown' then mp2[i].l[j,0].Caption:=TeamFullNameByID(m[x].Team1) else mp2[i].l[j,0].Caption:=FormatESMTeam(m[x].ESMTeam1);
          if TeamFullNameByID(m[x].Team2)<>'unknown' then mp2[i].l[j,2].Caption:=TeamFullNameByID(m[x].Team2) else mp2[i].l[j,2].Caption:=FormatESMTeam(m[x].ESMTeam2);

          if TeamFullNameByID(m[x].Team1)<>'unknown' then mp2[i].il[j,0].Picture.LoadFromFile('photos\'+m[x].Team1+'.png') else mp2[i].il[j,0].Picture.LoadFromFile('photos\UNK.png');
          if TeamFullNameByID(m[x].Team2)<>'unknown' then mp2[i].il[j,1].Picture.LoadFromFile('photos\'+m[x].Team2+'.png') else mp2[i].il[j,1].Picture.LoadFromFile('photos\UNK.png');
        end;
  end;
end;

procedure TF1.Resetallmatches1Click(Sender: TObject);
var i, k: word;
begin
  if MessageDlg('This will set all matches as "not disputed". Are you sure you want to do this?', mtWarning, [mbYes, mbCancel], 0)=mrYes then
    begin
      for i:=0 to nm-1 do m[i].Disputed:=false;
      for i:=24 to nm-1 do begin m[i].Team1:='UNK'; m[i].Team2:='UNK' end;
      SaveDatatoXML('matches');
      F1.refBClick(Resetallmatches1); if FM.Showing then FM.mlbChange(Resetallmatches1);
      if FT.Showing then begin for k:=1 to ne do if TPanel(FT.FindComponent('Panel'+inttostr(k))).Caption=FT.Label1.Caption then break; FT.Panel1Click(FT.FindComponent('Panel'+inttostr(k))) end;
    end
end;

procedure TF1.showProgEdBClick(Sender: TObject);
begin
  pEdF.Show
end;

end.
