object FVen: TFVen
  Left = 0
  Top = 0
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsNone
  Caption = 'Venues'
  ClientHeight = 437
  ClientWidth = 952
  Color = 8396895
  Font.Charset = ANSI_CHARSET
  Font.Color = clWhite
  Font.Height = -19
  Font.Name = 'Asap'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  ScreenSnap = True
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 22
  object Panel9: TPanel
    Left = 8
    Top = 8
    Width = 937
    Height = 423
    BevelInner = bvSpace
    BevelOuter = bvNone
    BevelWidth = 5
    BorderWidth = 5
    TabOrder = 0
    object img: TImage
      Left = 182
      Top = 31
      Width = 448
      Height = 362
      Center = True
      Picture.Data = {
        0954506E67496D61676589504E470D0A1A0A0000000D49484452000000400000
        002B0806000000C85A8E470000000467414D410000B18E7CFB51930000000970
        48597300000B1200000B1201D2DD7EFC000001364944415478DAEDD7BD4AC540
        1005E0339B44AE3FC9154C992E106B2BC1D2BC8CEFE22BADA560A58570412E16
        B7B010B450B1B0D8C449107C834C316721CC96271FB3EC8E745D779DE7F9594A
        09DE56966590B66DEF8AA2381F86C13ACFE22B8400699A26EAA6F708202290BA
        AE6780711CADF3D80094651975E31740CFFF0C601DC61041A256BF00FA118000
        04200001AC8398019C2A40A600FE1EC23A0B4C001B05A814E0D33ACDC26B6AFD
        83A9EEBC033C13800004200001084000025827220001086002B05580B5027C58
        273200389CEAFB15E2D10AFDF78F75A4E5015685D6E109512AF4707A0624DD23
        8612FDF8659D68E1FF9F00F609400002A40705A83C033C7AEF808DE30E98AFC1
        AD02AC15C0D93BE01F60F7D7016E015EBC03BCE2261CE3124EA721496FB80D27
        B880B3616806D8037E01F1578C371868CA9B0000000049454E44AE426082}
    end
    object Label1: TLabel
      Left = 636
      Top = 73
      Width = 88
      Height = 33
      Caption = 'Label1'
      Font.Charset = ANSI_CHARSET
      Font.Color = 16689632
      Font.Height = -29
      Font.Name = 'Asap'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 636
      Top = 141
      Width = 87
      Height = 22
      Caption = 'Located in'
    end
    object Label3: TLabel
      Left = 729
      Top = 141
      Width = 56
      Height = 22
      Caption = 'Label3'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8454143
      Font.Height = -19
      Font.Name = 'Asap'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 729
      Top = 169
      Width = 56
      Height = 22
      Caption = 'Label4'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8454143
      Font.Height = -19
      Font.Name = 'Asap'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object flagImg: TImage
      Left = 701
      Top = 174
      Width = 22
      Height = 15
      AutoSize = True
      Center = True
      Proportional = True
      Stretch = True
    end
    object Label5: TLabel
      Left = 636
      Top = 215
      Width = 76
      Height = 22
      Caption = 'Capacity:'
    end
    object Label6: TLabel
      Left = 718
      Top = 215
      Width = 56
      Height = 22
      Caption = 'Label6'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8454143
      Font.Height = -19
      Font.Name = 'Asap'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel
      Left = 636
      Top = 261
      Width = 207
      Height = 22
      Caption = 'more information online'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -19
      Font.Name = 'Asap'
      Font.Style = [fsItalic]
      ParentFont = False
      OnClick = Label7Click
      OnMouseEnter = Panel1MouseEnter
      OnMouseLeave = Panel1MouseLeave
    end
    object Label8: TLabel
      Left = 636
      Top = 281
      Width = 185
      Height = 22
      Caption = 'search Google Images'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -19
      Font.Name = 'Asap'
      Font.Style = [fsItalic]
      ParentFont = False
      OnClick = Label8Click
      OnMouseEnter = Panel1MouseEnter
      OnMouseLeave = Panel1MouseLeave
    end
    object Panel1: TPanel
      Left = 26
      Top = 31
      Width = 150
      Height = 40
      BevelOuter = bvNone
      Caption = 'Teams'
      Color = 6227264
      ParentBackground = False
      TabOrder = 0
      OnClick = Panel1Click
      OnMouseEnter = Panel1MouseEnter
      OnMouseLeave = Panel1MouseLeave
    end
    object Panel2: TPanel
      Left = 26
      Top = 77
      Width = 150
      Height = 40
      BevelOuter = bvNone
      Caption = 'Teams'
      Color = 6227264
      ParentBackground = False
      TabOrder = 1
      OnClick = Panel1Click
      OnMouseEnter = Panel1MouseEnter
      OnMouseLeave = Panel1MouseLeave
    end
    object Panel3: TPanel
      Left = 26
      Top = 123
      Width = 150
      Height = 40
      BevelOuter = bvNone
      Caption = 'Teams'
      Color = 6227264
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -19
      Font.Name = 'Asap'
      Font.Style = []
      ParentBackground = False
      ParentFont = False
      TabOrder = 2
      OnClick = Panel1Click
      OnMouseEnter = Panel1MouseEnter
      OnMouseLeave = Panel1MouseLeave
    end
    object Panel4: TPanel
      Left = 26
      Top = 169
      Width = 150
      Height = 40
      BevelOuter = bvNone
      Caption = 'Teams'
      Color = 6227264
      ParentBackground = False
      TabOrder = 3
      OnClick = Panel1Click
      OnMouseEnter = Panel1MouseEnter
      OnMouseLeave = Panel1MouseLeave
    end
    object Panel5: TPanel
      Left = 26
      Top = 215
      Width = 150
      Height = 40
      BevelOuter = bvNone
      Caption = 'Teams'
      Color = 6227264
      ParentBackground = False
      TabOrder = 4
      OnClick = Panel1Click
      OnMouseEnter = Panel1MouseEnter
      OnMouseLeave = Panel1MouseLeave
    end
    object Panel6: TPanel
      Left = 26
      Top = 261
      Width = 150
      Height = 40
      BevelOuter = bvNone
      Caption = 'Teams'
      Color = 6227264
      ParentBackground = False
      TabOrder = 5
      OnClick = Panel1Click
      OnMouseEnter = Panel1MouseEnter
      OnMouseLeave = Panel1MouseLeave
    end
    object Panel7: TPanel
      Left = 26
      Top = 307
      Width = 150
      Height = 40
      BevelOuter = bvNone
      Caption = 'Teams'
      Color = 6227264
      ParentBackground = False
      TabOrder = 6
      OnClick = Panel1Click
      OnMouseEnter = Panel1MouseEnter
      OnMouseLeave = Panel1MouseLeave
    end
    object Panel8: TPanel
      Left = 26
      Top = 353
      Width = 150
      Height = 40
      BevelOuter = bvNone
      Caption = 'Teams'
      Color = 6227264
      ParentBackground = False
      TabOrder = 7
      OnClick = Panel1Click
      OnMouseEnter = Panel1MouseEnter
      OnMouseLeave = Panel1MouseLeave
    end
    object exitB: TPanel
      Left = 651
      Top = 333
      Width = 192
      Height = 49
      Cursor = crHandPoint
      BevelOuter = bvNone
      Caption = 'Close'
      Color = 6227264
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -24
      Font.Name = 'Asap'
      Font.Style = [fsBold]
      ParentBackground = False
      ParentFont = False
      TabOrder = 8
      OnClick = exitBClick
      OnMouseEnter = Panel1MouseEnter
      OnMouseLeave = Panel1MouseLeave
    end
  end
end
