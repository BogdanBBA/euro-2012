unit matchEditor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TFME = class(TForm)
    Panel9: TPanel;
    saveL: TLabel;
    exitB: TPanel;
    matchIDL: TLabel;
    Dch: TCheckBox;
    Label5: TLabel;
    closeT: TTimer;
    Panel2: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    rb4: TRadioButton;
    rb3: TRadioButton;
    rb2: TRadioButton;
    rb1: TRadioButton;
    t1L: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    t2L: TLabel;
    Edit3: TEdit;
    Edit4: TEdit;
    Panel1: TPanel;
    SCT1L: TLabel;
    SCT2L: TLabel;
    SCrb2: TRadioButton;
    SCrb1: TRadioButton;
    Label6: TLabel;
    procedure FormShow(Sender: TObject);
    procedure exitBMouseEnter(Sender: TObject);
    procedure exitBMouseLeave(Sender: TObject);
    procedure exitBClick(Sender: TObject);
    procedure saveLClick(Sender: TObject);
    procedure rb1Click(Sender: TObject);
    procedure closeTTimer(Sender: TObject);
    procedure DchClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

type TMRT=record
  s: string;
  a, b: byte;
end;

var
  FME: TFME;
  mrt: TMRT;

implementation

{$R *.dfm}

uses DataTypes, functii, matches, teams;

procedure TFME.closeTTimer(Sender: TObject);
begin closeT.Enabled:=false; FMe.exitBClick(closeT) end;

procedure TFME.DchClick(Sender: TObject);
begin
  if Dch.Checked then if (m[xm].Team1='UNK') or (m[xm].Team2='UNK') then begin showmessage('You may not edit this match because you don''t know yet who''s playing!'); Dch.Checked:=false; exit end;
  Panel2.Enabled:=Dch.Checked; Label6.Visible:=not Dch.Checked;
  if Dch.Checked then
    begin Label5.Caption:='Disputed match'; rb2.Enabled:=xm>15; rb3.Enabled:=xm>15 end
    else Label5.Caption:='Match not disputed'
end;

procedure TFME.exitBClick(Sender: TObject);
begin
  FM.mlbChange(exitB);
  FME.Close
end;

procedure TFME.exitBMouseEnter(Sender: TObject);
begin
  if Sender is TLabel then TLabel(Sender).Font.Color:=clYellow
  else if Sender is TPanel then TPanel(Sender).Font.Color:=clYellow
end;

procedure TFME.exitBMouseLeave(Sender: TObject);
begin
  if Sender is TLabel then TLabel(Sender).Font.Color:=clWhite
  else if Sender is TPanel then TPanel(Sender).Font.Color:=clWhite
end;

procedure TFME.rb1Click(Sender: TObject);
begin
  Edit3.Enabled:=rb3.Checked; Edit4.Enabled:=rb3.Checked;
  SCrb1.Enabled:=rb4.Checked; SCrb2.Enabled:=rb4.Checked
end;

procedure DecodeMRT(s: string; var x: TMRT);
begin
  if pos(':', s)=0 then begin x.s:=s; x.a:=0; x.b:=0 end
  else
    begin
      x.s:=copy(s, 1, pos(':', s)-1);
      if pos('-', s)=0 then begin x.a:=strtoint(copy(s, pos(':', s)+1, 3)); x.b:=0 end
      else begin x.a:=strtoint(copy(s, pos(':', s)+1, pos('-', s)-pos(':', s)-1)); x.b:=strtoint(copy(s, pos('-', s)+1, 3)) end
    end;
end;

procedure TFME.FormShow(Sender: TObject);
begin
  Dch.Checked:=false;
  rb1Click(rb1);
  matchIDL.Caption:='Editing match ID='+inttostr(xm+1)+' result'; Dch.Checked:=m[xm].Disputed; DchClick(FME);
  t1L.Caption:=TeamFullNameByID(m[xm].Team1); t2L.Caption:=TeamFullNameByID(m[xm].Team2); SCT1L.Caption:=t1L.Caption; SCT2L.Caption:=t2L.Caption;
  Edit1.Text:=inttostr(m[xm].Goals1); Edit2.Text:=inttostr(m[xm].Goals2); Edit3.Text:='0'; Edit4.Text:='0';
  DecodeMRT(m[xm].MatchResultType, mrt);
  TRadioButton(FindComponent('rb'+inttostr(strtocase(mrt.s, ['normal', 'extra', 'shootout', 'special'])))).Checked:=true;
  case strtocase(mrt.s, ['normal', 'extra', 'shootout', 'special']) of
  3: begin Edit3.Text:=inttostr(mrt.a); Edit4.Text:=inttostr(mrt.b) end;
  4: try TRadioButton(FindComponent('SCrb'+inttostr(mrt.a))).Checked:=true except SCrb1.Checked:=true end;
  end;
end;

procedure TFME.saveLClick(Sender: TObject);
var k, q: word;
begin
  for k:=1 to 4 do if TRadioButton(FindComponent('rb'+inttostr(k))).Checked then break;
  //test for errors
  try q:=strtoint(Edit1.Text) except showmessage('Invalid home goals value.'); exit end;
  try q:=strtoint(Edit2.Text) except showmessage('Invalid away goals value.'); exit end;
  if k=3 then
    begin
      try q:=strtoint(Edit3.Text) except showmessage('Invalid penalty shoot-out home points (goals) value.'); exit end;
      try q:=strtoint(Edit4.Text) except showmessage('Invalid penalty shoot-out home points (goals) value.'); exit end;
    end;
  //context tests
  if xm<=23 then //Gr
    begin
      if k in [2,3] then
        if MessageDlg('Finishing a group stage match like this is highly irregular; are you sure yo want to continue?', mtConfirmation, [mbYes, mbCancel], 0)=mrYes then
          begin
            if k=2 then begin if Edit1.Text=Edit2.Text then begin showmessage('The match must have a winner!'+nl+'(The goals can not be the same for both teams if the match has ended in normal or extra time)'); exit end end
            else
              begin
                if Edit1.Text<>Edit2.Text then begin showmessage('You can not end up in a penalty shoot-out if the score is not a draw!'); exit end
                else if Edit3.Text=Edit4.Text then begin showmessage('The match must have a winner!'+nl+'(The penalty shoot-out can not end in a draw)'); exit end
              end
          end;
    end
  else //Elim
    begin
      if k in [1,2] then if Edit1.Text=Edit2.Text then begin showmessage('The match must have a winner!'+nl+'(The goals can not be the same for both teams if the match has ended in normal or extra time)'); exit end;
      if k=3 then
        begin
          if Edit1.Text<>Edit2.Text then begin showmessage('You can not end up in a penalty shoot-out if the score is not a draw!'); exit end
          else if Edit3.Text=Edit4.Text then begin showmessage('The match must have a winner!'+nl+'(The penalty shoot-out can not end in a draw)'); exit end
        end
    end;
  //save
  m[xm].Disputed:=Dch.Checked;
  m[xm].Goals1:=strtoint(Edit1.Text); m[xm].Goals2:=strtoint(Edit2.Text);
  if SCrb1.Checked then q:=1 else q:=2;
  case k of
  1: m[xm].MatchResultType:='normal';
  2: m[xm].MatchResultType:='extra';
  3: m[xm].MatchResultType:='shootout:'+Edit3.Text+'-'+Edit4.Text;
  4: m[xm].MatchResultType:='special:'+inttostr(q);
  end;
  //
  SaveDatatoXML('matches');
  saveL.Font.Color:=$0000FF80;
  closeT.Enabled:=true
end;

end.

