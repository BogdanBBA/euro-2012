program Euro2012;

uses
  Forms,
  Euro12_pas in 'Euro12_pas.pas' {F1},
  functii in 'functii.pas',
  DataTypes in 'DataTypes.pas',
  progEditor in 'progEditor.pas' {pEdF},
  venues in 'venues.pas' {FVen},
  matches in 'matches.pas' {FM},
  matchEditor in 'matchEditor.pas' {FME},
  teams in 'teams.pas' {FT},
  MatchTime in 'MatchTime.pas' {FMT};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TF1, F1);
  Application.CreateForm(TpEdF, pEdF);
  Application.CreateForm(TFVen, FVen);
  Application.CreateForm(TFM, FM);
  Application.CreateForm(TFME, FME);
  Application.CreateForm(TFT, FT);
  Application.CreateForm(TFMT, FMT);
  Application.Run;
end.
