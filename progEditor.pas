unit progEditor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls;

type
  TpEdF = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    mlb: TListBox;
    Label3: TLabel;
    Label4: TLabel;
    stgE: TEdit;
    Label5: TLabel;
    ccb: TComboBox;
    dtp1: TDateTimePicker;
    Label6: TLabel;
    dtp2: TDateTimePicker;
    Label7: TLabel;
    t1cb: TComboBox;
    Label8: TLabel;
    t2cb: TComboBox;
    saveMB: TButton;
    procedure FormShow(Sender: TObject);
    procedure mlbClick(Sender: TObject);
    procedure saveMBClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  pEdF: TpEdF;

implementation

{$R *.dfm}

uses functii, DataTypes, Euro12_pas;

procedure TpEdF.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  F1.Show;
end;

procedure TpEdF.FormShow(Sender: TObject);
var i: word;
begin
  F1.Hide;
  mlb.Clear; for i:=0 to nm-1 do mlb.Items.Add('Match '+inttostr(i)+' ('+m[i].Stage+'): '+m[i].Team1+'-'+m[i].Team2);
  ccb.Clear; for i:=0 to nv-1 do ccb.Items.Add(v[i].City);
  t1cb.Clear; for i:=0 to ne-1 do t1cb.Items.Add(t[i].ShortName); t2cb.Items:=t1cb.Items;
end;

procedure TpEdF.mlbClick(Sender: TObject);
begin
  Label3.Caption:='Match id='+inttostr(mlb.ItemIndex);
  stgE.Text:=m[mlb.ItemIndex].Stage;
  dtp1.Date:=m[mlb.ItemIndex].When;
  dtp2.DateTime:=m[mlb.ItemIndex].When;
  ccb.ItemIndex:=ccb.Items.IndexOf(m[mlb.ItemIndex].City);
  t1cb.ItemIndex:=t1cb.Items.IndexOf(m[mlb.ItemIndex].Team1);
  t2cb.ItemIndex:=t2cb.Items.IndexOf(m[mlb.ItemIndex].Team2);
end;

procedure TpEdF.saveMBClick(Sender: TObject);
begin
  m[mlb.ItemIndex].Stage:=stgE.Text;
  m[mlb.ItemIndex].When:=trunc(dtp1.DateTime)+frac(dtp2.Time);
  m[mlb.ItemIndex].City:=ccb.Text;
  m[mlb.ItemIndex].Team1:=t1cb.Text;
  m[mlb.ItemIndex].Team2:=t2cb.Text;
  //m[mlb.ItemIndex].Disputed:=false;
  savedatatoxml('matches');
  mlb.Items[mlb.ItemIndex]:='Match '+inttostr(mlb.ItemIndex)+' ('+m[mlb.ItemIndex].Stage+'): '+m[mlb.ItemIndex].Team1+'-'+m[mlb.ItemIndex].Team2
end;

end.
