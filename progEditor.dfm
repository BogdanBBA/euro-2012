object pEdF: TpEdF
  Left = 0
  Top = 0
  Caption = 'Editor'
  ClientHeight = 579
  ClientWidth = 513
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clBlack
  Font.Height = -13
  Font.Name = 'Verdana'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 259
    Height = 16
    Caption = 'To be only used in programming phase!'
  end
  object Label2: TLabel
    Left = 8
    Top = 30
    Width = 61
    Height = 16
    Caption = 'Matches'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold, fsUnderline]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 280
    Top = 30
    Width = 48
    Height = 16
    Caption = 'Label3'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold, fsItalic]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 280
    Top = 48
    Width = 39
    Height = 16
    Caption = 'Stage'
  end
  object Label5: TLabel
    Left = 280
    Top = 142
    Width = 26
    Height = 16
    Caption = 'City'
  end
  object Label6: TLabel
    Left = 280
    Top = 94
    Width = 93
    Height = 16
    Caption = 'Date and time'
  end
  object Label7: TLabel
    Left = 280
    Top = 188
    Width = 49
    Height = 16
    Caption = 'Team 1'
  end
  object Label8: TLabel
    Left = 280
    Top = 234
    Width = 49
    Height = 16
    Caption = 'Team 2'
  end
  object mlb: TListBox
    Left = 8
    Top = 48
    Width = 259
    Height = 523
    TabOrder = 0
    OnClick = mlbClick
  end
  object stgE: TEdit
    Left = 280
    Top = 64
    Width = 225
    Height = 24
    TabOrder = 1
  end
  object ccb: TComboBox
    Left = 280
    Top = 158
    Width = 225
    Height = 24
    Style = csDropDownList
    Sorted = True
    TabOrder = 2
  end
  object dtp1: TDateTimePicker
    Left = 280
    Top = 112
    Width = 129
    Height = 24
    Date = 41038.617072430560000000
    Time = 41038.617072430560000000
    TabOrder = 3
  end
  object dtp2: TDateTimePicker
    Left = 415
    Top = 112
    Width = 90
    Height = 24
    Date = 41038.791666666660000000
    Time = 41038.791666666660000000
    Kind = dtkTime
    TabOrder = 4
  end
  object t1cb: TComboBox
    Left = 280
    Top = 204
    Width = 225
    Height = 24
    Style = csDropDownList
    DropDownCount = 32
    Sorted = True
    TabOrder = 5
  end
  object t2cb: TComboBox
    Left = 280
    Top = 250
    Width = 225
    Height = 24
    Style = csDropDownList
    DropDownCount = 32
    Sorted = True
    TabOrder = 6
  end
  object saveMB: TButton
    Left = 280
    Top = 288
    Width = 225
    Height = 33
    Caption = 'Save'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 7
    OnClick = saveMBClick
  end
end
