unit functii;

interface

uses Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ShlObj, URLMon, DateUtils, DataTypes;

type ByteSet=set of byte;

function StringToCompetition(s: string): TCompetition;
function CompetitionToString(x: TCompetition): string;
function StringToCompetitionResult(s: string): TCompetitionResult;
function CompetitionResultToString(x: TCompetitionResult): string;
function StrToWhen(s: string): TDateTime;
function Golaveraj(x: integer): string;
function TeamFullNameByID(s: string): string;
function MatchScore(x: byte): string;
function MatchResultType(x: byte): string;
function MatchWinner(x: byte): string;
function Stage(x: string): string;
function GSMatchIDbyLabelIJ(ESPanelX, MatchLabelX: word): word;
function ESMatchIDbyLabelIJ(ESPanelX, MatchLabelX: word): word;
function FormatESMTeam(x: string): string;
function HtmlToColor(Color: string): TColor;
function min(x, y: longint): longint;
function max(x, y: longint): longint;
function setofbyte_count(x: ByteSet): word;
function downcase(c: char): char; Overload;
function downcase(c: string): string; Overload;
function fileword(s: string): string;
procedure clean_str(var s: string; Edges, NoMoreThanOneSpaceAtATime: boolean);
function inttoboolean(x: word): boolean;
function booleantostr(x: boolean): string;
function booleantoyesno(x: boolean): string;
function strtocase(Selector : string; CaseList: array of string): Integer;
function upcase(s: string): string; Overload;
function tkb(y: longint): string;
function gender(sex, word_id: byte; capitalize: boolean): string;
function CalculateAge(Birthday, CurrentDate: string): Integer;
function number_suffix(x: longint): string;
function noext(s: string): string;
function GetDirSize(dir: string; subdir: Boolean): Longint;
function song_length(millisec: longint): string;
function tdhms2b(value: int64): string;
function FYear(x: integer): string;
function FontToStr(Font: TFont): string;
function StrToFont(const s: string; var Font: TFont): boolean;
function nextd(x: word): word;
function prevd(x: word): word;
function GetDesktopFolder: string;
function download(web_addr, dest_name: string): boolean;
function web_str(s: string): string;

implementation

function StringToCompetition(s: string): TCompetition;
begin
  case strtocase(s, ['Euro', 'World Cup']) of
  1: result:=CEuro;
  2: result:=CWorldCup
  else result:=CUnknown
  end;
//  log('StringToCompetition(s="'+s+'") resulted in "'+CompetitionToString(result)+'"')
end;

function CompetitionToString(x: TCompetition): string;
begin
  case x of
  CEuro: result:='Euro';
  CWorldCup: result:='World Cup'
  else result:='unknown'
  end;
//  log('CompetitionToString(x); obtained "'+result+'"')
end;

function StringToCompetitionResult(s: string): TCompetitionResult;
begin
  case strtocase(s, ['Winner', 'Runner-up', 'Bronze', 'Quarter-finals']) of
  1: result:=CRWinner;
  2: result:=CRRunnerUp;
  3: result:=CRBronze;
  4: result:=CRQuarterFinals
  else result:=CRUnknown
  end;
  //log('StringToCompetitionResult(s="'+s+'") resulted in "'+CompetitionResultToString(result)+'"')
end;

function CompetitionResultToString(x: TCompetitionResult): string;
begin
  case x of
  CRWinner: result:='Winner';
  CRRunnerUp: result:='Runner-up';
  CRBronze: result:='Bronze';
  CRQuarterFinals: result:='Quarter-finals'
  else result:='unknown'
  end;
  //log('CompetitionResultToString(x); obtained "'+result+'"')
end;

function StrToWhen(s: string): TDateTime;
var y, m, d, h, n: word;
begin
  try y:=strtoint(copy(s, 1, 4)); m:=strtoint(s[6]+s[7]); d:=strtoint(s[9]+s[10]); h:=strtoint(s[12]+s[13]); n:=strtoint(s[15]+s[16])
  except y:=1900; m:=1; d:=1; h:=0; n:=0 end;
  result := encodedatetime(y, m, d, h, n, 0, 0)
end;

function Golaveraj(x: integer): string;
var s: string;
begin
  if x=0 then s:='' else if x>0 then s:='+' else s:='-';
  result := s+inttostr(abs(x))
end;

function TeamFullNameByID(s: string): string;
var i: word;
begin
  try
  result:='unknown';
  for i:=0 to ne-1 do
    if t[i].ShortName=s then
      begin result:=t[i].Name; exit end
  except on E:Exception do showmessage('TeamFullNameByID() ERROR, s='+s+dnl+E.ClassName+': '+E.Message) end;
end;

function MatchScore(x: byte): string;
begin
  if not m[x].Disputed then result:='-'
  else
    begin
      result := inttostr(m[x].Goals1)+'-'+inttostr(m[x].Goals2)
    end;
end;

function MatchResultType(x: byte): string;
var y: byte; ys: string;
begin
  if not m[x].Disputed then result:='match has not yet been disputed'
  else
    begin
      if m[x].MatchResultType='normal' then result:='finished in regular match time'
      else if m[x].MatchResultType='extra' then result:='finished in extra time'
      else
        begin
          if pos('shootout:', m[x].MatchResultType)<>0 then
            try result:='finished at penalty shoot-out, '+copy(m[x].MatchResultType, 10, 5) except result:='MatchResultType() ERROR (shootout section), source="'+m[x].MatchResultType+'"' end
          else if pos('special:', m[x].MatchResultType)<>0 then
            try y:=strtoint(copy(m[x].MatchResultType, 9, 1)); if y=1 then ys:=TeamFullNameByID(m[x].Team1) else ys:=TeamFullNameByID(m[x].Team2); result:='special circumstances, '+ys+' won' except result:='MatchResultType() ERROR (special section), source="'+m[x].MatchResultType+'"' end
        end;
    end;
end;

function MatchWinner(x: byte): string; //will return nothing if the match is tied and non-eliminatory, or if it hasn't been disputed
var xs: string; gx, gy: word;
begin
  result:=''; if not m[x].Disputed then exit;
  if m[x].MatchResultType='normal' then
    begin
      if m[x].Goals1>m[x].Goals2 then begin result:=m[x].Team1; exit end
      else if m[x].Goals1<m[x].Goals2 then begin result:=m[x].Team2; exit end
      else begin if x<24 then exit else showmessage('MatchWinner(xMatch='+inttostr(x)+', '+m[x].Team1+'-'+m[x].Team2+') ERROR: tied result for eliminatory match (regular-time)') end
    end
  else if m[x].MatchResultType='extra' then
    begin
      if m[x].Goals1>m[x].Goals2 then begin result:=m[x].Team1; exit end
      else if m[x].Goals1<m[x].Goals2 then begin result:=m[x].Team2; exit end
      else showmessage('MatchWinner(xMatch='+inttostr(x)+', '+m[x].Team1+'-'+m[x].Team2+') ERROR: tied result for eliminatory match (extra-time)')
    end
  else if pos('shootout', m[x].MatchResultType)<>0 then
    begin
      try xs:=m[x].MatchResultType; gx:=strtoint(copy(xs, pos(':', xs)+1, pos('-', xs)-pos(':', xs)-1)); gy:=strtoint(copy(xs, pos('-', xs)+1, 3)) except on E:Exception do begin showmessage('MatchWinner(xMatch='+inttostr(x)+', '+m[x].Team1+'-'+m[x].Team2+') ERROR: invalid shootout goal values, string="'+xs+'"'+dnl+E.ClassName+': '+E.Message); exit end end;
      if gx>gy then begin result:=m[x].Team1; exit end
      else if gx<gy then begin result:=m[x].Team2; exit end
      else showmessage('MatchWinner(xMatch='+inttostr(x)+', '+m[x].Team1+'-'+m[x].Team2+') ERROR: tied result for eliminatory match shoot-out ('+inttostr(gx)+'-'+inttostr(gy)+')')
    end
  else if pos('special', m[x].MatchResultType)<>0 then
    begin
      xs:=m[x].MatchResultType[length(m[x].MatchResultType)];
      if xs='1' then begin result:=m[x].Team1; exit end
      else if xs='2' then begin result:=m[x].Team2; exit end
      else showmessage('MatchWinner(xMatch='+inttostr(x)+', '+m[x].Team1+'-'+m[x].Team2+') ERROR: invalid special circumstances winner number ID="'+xs+'"')
    end
end;

function Stage(x: string): string;
begin
  if pos('group', x)<>0 then result:='Group '+x[6]+' match'
  else if x='QF' then result:='Quarter-final'
  else if x='SF' then result:='Semi-final'
  else if x='F' then result:='Final'
  else result:='unknown stage'
end;

function GSMatchIDbyLabelIJ(ESPanelX, MatchLabelX: word): word;
var r: word;
begin
  result := ESPanelX*2+(MatchLabelX div 2)*8+MatchLabelX mod 2
end;

function ESMatchIDbyLabelIJ(ESPanelX, MatchLabelX: word): word;
var r: word;
begin
  r:=0;
  case ESPanelX of
  0: r:=24+MatchLabelX;
  1: r:=28+MatchLabelX;
  2: r:=30
  else showmessage('ESMatchIDbyLabelIJ ERROR: Invalid (ESPanelX, MatchLabelX) combination: ESPanelX='+inttostr(ESPanelX)+', MatchLabelX='+inttostr(MatchLabelX))
  end;
  result := r
end;

function FormatESMTeam(x: string): string;
begin
  if length(x)<>2 then begin showmessage('FormatESMTeam ERROR: Inavlid x="'+x+'"'); exit end;
  try
  if charinset(upcase(x[1]), ['A', 'B', 'C', 'D']) then
    begin
      if x[2]='1' then result:='Winner (Group '+x[1]+')' else if x[2]='2' then result:='Runner-up (Group '+x[1]+')'
      else result:='FormatESMTeam error: Invalid x="'+x+'"'
    end
  else result:='Winner (Match '+inttostr(strtoint(x))+')'
  except on E:Exception do showmessage('FormatESMTeam ERROR; x="'+x+'"'+dnl+E.ClassName+': '+E.Message) end
end;

function HtmlToColor(Color: string): TColor;
begin
  Result := StringToColor('$' + Copy(Color, 6, 2) + Copy(Color, 4, 2) + Copy(Color, 2, 2));
end;

function min(x, y: longint): longint;
begin if x>y then min:=y else min:=x end;

function max(x, y: longint): longint;
begin if x<y then max:=y else max:=x end;

function setofbyte_count(x: ByteSet): word;
var i, n: word;
begin n:=0; for i:=low(byte) to high(byte) do if i in x then inc(n); result:=n end;

function downcase(c: char): char; Overload;
begin if ((c>='A') and (c<='Z')) then downcase:=chr(ord(c)+32) else downcase:=c; end;

function downcase(c: string): string; Overload;
var t: string; i: word;
begin t:=''; for i:=1 to length(c) do if ((c[i]>='A') and (c[i]<='Z')) then t:=t+chr(ord(c[i])+32) else t:=t+c[i]; downcase:=t; end;

function fileword(s: string): string;
var rez: string; i: word;
begin rez:=''; s:=downcase(s); for i:=1 to length(s) do if charinset(s[i], ['0'..'9', 'a'..'z', 'A'..'Z']) then rez:=rez+s[i]; fileword:=rez end;

procedure clean_str(var s: string; Edges, NoMoreThanOneSpaceAtATime: boolean);
var t, x, y: string;
begin
  if s='' then exit; t:=s;
  if Edges=true then
    begin while t[1]=' ' do delete(t, 1, 1); while t[length(t)]=' ' do delete(t, length(t), 1) end;
  if NoMoreThanOneSpaceAtATime then
    begin
      x:=t;
      while length(x)>1 do begin if ((x[1]<>' ') or ((x[1]=' ') and (x[2]<>' '))) then y:=y+x[1]; delete(x, 1, 1) end;
      t:=y+x[1];
    end; s:=t
end;

//

function inttoboolean(x: word): boolean;
begin inttoboolean:=false; if x=0 then inttoboolean:=false else if x=1 then inttoboolean:=true else begin showmessage('ERROR: inttoboolean: invalid integer; x='+inttostr(x)); exit end end;

function booleantostr(x: boolean): string;
begin if x=true then result:='true' else result:='false' end;

function booleantoyesno(x: boolean): string;
begin if x=true then result:='yes' else result:='no' end;

function strtocase(Selector : string; CaseList: array of string): Integer;
var cnt: integer;
begin
  Result:=0; //clean_str(selector, true, false);
  for cnt:=0 to Length(CaseList)-1 do
    begin
      //clean_str(caselist[cnt], true, false);
      if CompareText(Selector, CaseList[cnt]) = 0 then
        begin Result:=cnt+1; Break end
    end
end;

function upcase(s: string): string; Overload;
var i: word;
begin
  for i:=1 to length(s) do s[i]:=upcase(s[i]); upcase:=s;
end;

function tkb(y: longint): string;
var x: real;
begin
  if y<=0 then tkb:='0 B' else
  if ((y>=1) and (y<=999)) then
    begin tkb:=inttostr(y)+' B'; end
  else
    begin
      x:=y;
      if ((x>=1000) and (x<=999999)) then
        begin x:=x/1024; tkb:=formatfloat('0.00', x)+' KB'; end
      else
        begin x:=x/1024/1024; tkb:=formatfloat('0.00', x)+' MB'; end
    end;
end;

function gender(sex, word_id: byte; capitalize: boolean): string;
var r: string;
begin
  r:=''; if not (sex in [1, 2]) then begin showmessage('gender ERROR: sex<>[1, 2]; sex = '+inttostr(sex)); exit end;
  case word_id of
  1: if sex=1 then r:='he' else if sex=2 then r:='she';
  2: if sex=1 then r:='his' else if sex=2 then r:='her';
  3: if sex=1 then r:='man' else if sex=2 then r:='woman';
  else showmessage('gender ERROR: word_id invalid; word_id = '+inttostr(word_id));
  end;
  if (capitalize and (length(r)>0)) then r[1]:=upcase(r[1]);
  gender:=r
end;

function CalculateAge(Birthday, CurrentDate: string): Integer;
var Month, Day, Year, CurrentYear, CurrentMonth, CurrentDay: Word;
begin
  if length(birthday)<>10 then begin showmessage('calculateage ERROR, length('+birthday+') <> 10'); exit end;
  month:=strtoint(copy(birthday, 6, 2)); day:=strtoint(copy(birthday, 9, 2)); year:=strtoint(copy(birthday, 1, 4));
  Currentmonth:=strtoint(copy(CurrentDate, 6, 2)); Currentday:=strtoint(copy(CurrentDate, 9, 2)); Currentyear:=strtoint(copy(CurrentDate, 1, 4));
  if (Year = CurrentYear) and (Month = CurrentMonth) and (Day = CurrentDay) then Result := 0
  else
    begin
      Result := CurrentYear - Year;
      if (Month > CurrentMonth) then Dec(Result)
      else begin if Month = CurrentMonth then if (Day > CurrentDay) then Dec(Result); end;
    end;
  //if (Year = 1900) and (Month = 1) and (Day = 1) then Result := 0
end;

function number_suffix(x: longint): string;
var r: string;
begin
  r:='#ERR(suffix: unknown x mod 20='+inttostr(abs(x) mod 20)+')';
  case abs(x) mod 20 of
  1: r:='st';
  2: r:='nd';
  3: r:='rd';
  0, 4..19: r:='th';
  end;
  number_suffix := r
end;

function noext(s: string): string;
var k: word;
begin
  if pos('.', s)<>0 then begin k:=length(s); while s[k]<>'.' do inc(k, -1); delete(s, k, 10) end; noext:=s
end;

function GetDirSize(dir: string; subdir: Boolean): Longint;
var
  rec: TSearchRec;
  found: Integer;
begin
  Result := 0;
  if dir[Length(dir)] <> '\' then dir := dir + '\';
  found := FindFirst(dir + '*.*', faAnyFile, rec);
  while found = 0 do
  begin
    Inc(Result, rec.Size);
    if (rec.Attr and faDirectory > 0) and (rec.Name[1] <> '.') and (subdir = True) then
      Inc(Result, GetDirSize(dir + rec.Name, True));
    found := FindNext(rec);
  end;
  FindClose(rec);
end;

function song_length(millisec: longint): string;
begin song_length := inttostr(trunc(millisec/1000) div 60)+':'+formatfloat('00', trunc(millisec/1000) mod 60) end;

function tdhms2b(value: int64): string;
  function stri(value, typ: word): string;
  var r: string;
  begin
    if value=0 then
      begin stri:=''; exit end
    else
      begin
        case typ of
        0: r:='d';
        1: r:='hr';
        2: r:='min';
        3: r:='sec';
        4: r:='msec';
        end;
        stri:=', '+inttostr(value)+' '+r
      end
  end;
var nday, nhou, nmin: word; nsec, nmsec: longint; r: string; value2: int64;
begin
  value2:=value; value:=value div 1000;
  nday:=value div (3600*24); nhou:=(value mod (3600*24)) div 3600; nmin:=(value mod 3600) div 60; nsec:=value mod 60; nmsec:=value2 mod 1000;
  r:=stri(nday, 0)+stri(nhou, 1)+stri(nmin, 2)+stri(nsec, 3)+stri(nmsec, 4); tdhms2b:=copy(r, 3, length(r)-2)
end;

function FYear(x: integer): string;
begin
  if x=0 then FYear:='the year 0' else if x<0 then FYear:=inttostr(abs(x))+' BC' else FYear:='AD '+inttostr(x)
end;

function FontToStr(Font: TFont): string;
var sColor, sStyle : string;
begin
  sColor := '$' +IntToHex(ColorToRGB(Font.Color), 8);
  sStyle := IntToStr( byte(Font.Style) );
  result := Font.Name +'|'+ IntToStr(Font.Size) +'|'+sColor +'|'+sStyle;
end;

function StrToFont(const s: string; var Font: TFont): boolean;
var afont : TFont; Strs : TStringList;
begin
  try
    //log('StrToFont S="'+s+'"');
    afont := TFont.Create;
    if Font=nil then Font:=Tfont.Create;
    try
      afont.Assign(Font);
      Strs := TStringList.Create;
      try
        Strs.Text := StringReplace(s, '|', #10, [rfReplaceAll]);
        result := Strs.Count = 4;
        if result then
          begin
            afont.Name := Strs[0];
            afont.Size := StrToInt(Strs[1]);
            afont.Color := StrToInt(Strs[2]);
            afont.Style := TFontStyles(byte(StrToInt(Strs[3])));
          end;
        Font.Assign(afont);
      except on E:Exception do log('StrToFont error (inner):'+dnl+E.ClassName+' - "'+E.Message+'"') end
    except on E:Exception do log('StrToFont error (outer):'+dnl+E.ClassName+' - "'+E.Message+'"') end
  finally
    Strs.Free;
    afont.Free
  end
end;

function nextd(x: word): word;
begin if x=7 then nextd:=1 else nextd:=x+1 end;

function prevd(x: word): word;
begin if x=1 then prevd:=7 else prevd:=x-1 end;

function GetDesktopFolder: string;
var
 buf: array[0..MAX_PATH] of char;
 pidList: PItemIDList;
begin
 Result := 'No Desktop Folder found.';
 SHGetSpecialFolderLocation(Application.Handle, CSIDL_DESKTOP, pidList);
 if (pidList <> nil) then
  if (SHGetPathFromIDList(pidList, buf)) then
    Result := buf;
end;

function download(web_addr, dest_name: string): boolean;
begin
  try Result := UrlDownloadToFile(nil, PChar(web_addr), PChar(dest_name), 0, nil) = 0
  except Result := False end
end;

function web_str(s: string): string;
begin
  result := stringreplace(s, ' ', '+', [rfReplaceAll])
end;

end.

