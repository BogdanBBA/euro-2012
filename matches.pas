unit matches;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, PNGImage;

type
  TFM = class(TForm)
    Panel9: TPanel;
    mlb: TComboBox;
    refMlbB: TButton;
    exitB: TPanel;
    scoreL: TLabel;
    t2L: TLabel;
    t2I: TImage;
    t1L: TLabel;
    t1I: TImage;
    dateL: TLabel;
    stadiumL: TLabel;
    stageL: TLabel;
    matchResultTypeL: TLabel;
    editL: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure mlbChange(Sender: TObject);
    procedure refMlbBClick(Sender: TObject);
    procedure exitBClick(Sender: TObject);
    procedure exitBMouseEnter(Sender: TObject);
    procedure exitBMouseLeave(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure stadiumLClick(Sender: TObject);
    procedure editLClick(Sender: TObject);
    procedure t1LClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FM: TFM;
  xm: word;

implementation

{$R *.dfm}

uses Euro12_pas, functii, DataTypes, venues, matchEditor, teams, MatchTime;

procedure TFM.editLClick(Sender: TObject);
begin if FME.Showing then FME.OnShow(editL) else FME.Show; FME.SetFocus end;

procedure TFM.exitBClick(Sender: TObject);
var k: word;
begin
  F1.refBClick(exitB);
  if FT.Showing then
    begin for k:=1 to ne do if TPanel(FT.FindComponent('Panel'+inttostr(k))).Caption=FT.Label1.Caption then break; FT.Panel1Click(FT.FindComponent('Panel'+inttostr(k))) end;
  if FMT.Showing then FMT.mlbChange(exitB);
  FM.Close
end;

procedure TFM.exitBMouseEnter(Sender: TObject);
begin
  if Sender is TLabel then TLabel(Sender).Font.Color:=clYellow
  else if Sender is TPanel then TPanel(Sender).Font.Color:=clYellow
end;

procedure TFM.exitBMouseLeave(Sender: TObject);
begin
  if Sender is TLabel then TLabel(Sender).Font.Color:=clWhite
  else if Sender is TPanel then TPanel(Sender).Font.Color:=clWhite
end;

procedure TFM.FormCreate(Sender: TObject);
var i: word;
begin
  t1I.Picture.RegisterFileFormat('.PNG', 'port netw graph', TPngImage);
  t2I.Picture.RegisterFileFormat('.PNG', 'port netw graph', TPngImage);
  mlb.Clear; for i:=0 to nm-1 do mlb.Items.Add('Match '+inttostr(i)); refMlbBClick(FM)
end;

procedure TFM.FormShow(Sender: TObject);
begin
  refMlbBClick(FM);
  if mlb.ItemIndex=-1 then begin mlb.ItemIndex:=0; mlbChange(FM) end
end;

procedure TFM.mlbChange(Sender: TObject);
var k: word;
begin
  xm:=mlb.ItemIndex;
  //
  stageL.Caption:=stage(m[xm].Stage);
  dateL.Caption:=formatdatetime('dddd, d mmmm yyyy, "at" h:nn', m[xm].When);
  for k:=0 to nv-1 do if m[xm].City=v[k].City then break; stadiumL.Caption:=v[k].Name+', '+m[xm].City;
  //
  t1L.Caption:=TeamFullNameByID(m[xm].Team1);
  t2L.Caption:=TeamFullNameByID(m[xm].Team2);
  scoreL.Caption:=MatchScore(xm);
  matchResultTypeL.Caption:=MatchResultType(xm); if m[xm].MatchResultType='normal' then matchResultTypeL.Font.Color:=clWhite else matchResultTypeL.Font.Color:=stadiumL.Font.Color;
  if fileexists('photos\'+m[xm].Team1+'.png') then t1I.Picture.LoadFromFile('photos\'+m[xm].Team1+'.png') else t1I.Picture.LoadFromFile('photos\UNK.png');
  if fileexists('photos\'+m[xm].Team2+'.png') then t2I.Picture.LoadFromFile('photos\'+m[xm].Team2+'.png') else t2I.Picture.LoadFromFile('photos\UNK.png');
  //
  //if FME.Showing then FME.OnShow(mlb)
end;

procedure TFM.refMlbBClick(Sender: TObject);
var i: word;
begin
  try
  for i:=0 to nm-1 do
    mlb.Items[i]:='Match '+inttostr(i+1)+': '+TeamFullNameByID(m[i].Team1)+'-'+TeamFullNameByID(m[i].Team2)
  except on E:Exception do showmessage('TFM.refMlbBClick() ERROR, i='+inttostr(i)+' (match '+inttostr(i+1)+')'+dnl+E.ClassName+': '+E.Message) end;
end;

procedure TFM.stadiumLClick(Sender: TObject);
var k: word;
begin
  FVen.Show;
  for k:=0 to nv-1 do if m[mlb.ItemIndex].City=v[k].City then break;
  FVen.Panel1Click(FVen.FindComponent('Panel'+inttostr(k+1)))
end;

procedure TFM.t1LClick(Sender: TObject);
var x: word;
begin
  FT.Show;
  for x:=1 to 16 do
  if TPanel(FT.FindComponent('Panel'+inttostr(x))).Caption=TLabel(Sender).Caption then
    begin FT.Panel1Click(TPanel(FT.FindComponent('Panel'+inttostr(x)))); break end
end;

end.
