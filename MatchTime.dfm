object FMT: TFMT
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'FMT'
  ClientHeight = 415
  ClientWidth = 879
  Color = 8396895
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWhite
  Font.Height = -19
  Font.Name = 'Asap'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  ScreenSnap = True
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 22
  object Panel9: TPanel
    Left = 7
    Top = 8
    Width = 865
    Height = 401
    BevelInner = bvSpace
    BevelOuter = bvNone
    BevelWidth = 5
    BorderWidth = 5
    TabOrder = 0
    object t1L: TLabel
      Left = 136
      Top = 90
      Width = 40
      Height = 33
      Cursor = crHandPoint
      Caption = 't1L'
      Font.Charset = ANSI_CHARSET
      Font.Color = 16689632
      Font.Height = -29
      Font.Name = 'Asap'
      Font.Style = []
      ParentFont = False
      OnClick = t1LClick
    end
    object t1I: TImage
      Left = 34
      Top = 70
      Width = 96
      Height = 64
      Center = True
      Proportional = True
      Stretch = True
    end
    object col1I: TImage
      Left = 34
      Top = 140
      Width = 215
      Height = 18
    end
    object scoreL: TLabel
      Left = 365
      Top = 90
      Width = 124
      Height = 33
      Cursor = crHandPoint
      Alignment = taCenter
      AutoSize = False
      Caption = 'Label1'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8454143
      Font.Height = -29
      Font.Name = 'Asap'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = scoreLClick
    end
    object t2L: TLabel
      Left = 638
      Top = 89
      Width = 88
      Height = 33
      Cursor = crHandPoint
      Alignment = taRightJustify
      Caption = 'Label1'
      Font.Charset = ANSI_CHARSET
      Font.Color = 16689632
      Font.Height = -29
      Font.Name = 'Asap'
      Font.Style = []
      ParentFont = False
      OnClick = t1LClick
    end
    object t2I: TImage
      Left = 732
      Top = 70
      Width = 96
      Height = 64
      Center = True
      Proportional = True
      Stretch = True
    end
    object col2I: TImage
      Left = 613
      Top = 140
      Width = 215
      Height = 18
    end
    object Label7: TLabel
      Left = 388
      Top = 236
      Width = 80
      Height = 18
      Caption = 'Match time'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8454143
      Font.Height = -16
      Font.Name = 'Asap'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object timeL: TLabel
      Left = 34
      Top = 252
      Width = 794
      Height = 45
      Alignment = taCenter
      AutoSize = False
      Caption = 'timeL'
      Font.Charset = ANSI_CHARSET
      Font.Color = 16689632
      Font.Height = -40
      Font.Name = 'Asap'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object dateL: TLabel
      Left = 34
      Top = 194
      Width = 794
      Height = 22
      Alignment = taCenter
      AutoSize = False
      Caption = 'Label3'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8454143
      Font.Height = -19
      Font.Name = 'Asap'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object onL: TLabel
      Left = 797
      Top = 194
      Width = 31
      Height = 22
      Alignment = taRightJustify
      Caption = 'onL'
    end
    object exitB: TPanel
      Left = 636
      Top = 325
      Width = 192
      Height = 49
      Cursor = crHandPoint
      BevelOuter = bvNone
      Caption = 'Close'
      Color = 6227264
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -24
      Font.Name = 'Asap'
      Font.Style = [fsBold]
      ParentBackground = False
      ParentFont = False
      TabOrder = 0
      OnClick = exitBClick
      OnMouseEnter = exitBMouseEnter
      OnMouseLeave = exitBMouseLeave
    end
    object refMlbB: TButton
      Left = 34
      Top = 10
      Width = 89
      Height = 25
      Caption = 'refMlbB'
      TabOrder = 1
      Visible = False
      OnClick = refMlbBClick
    end
    object mlb: TComboBox
      Left = 34
      Top = 32
      Width = 479
      Height = 32
      Style = csDropDownList
      DropDownCount = 16
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -21
      Font.Name = 'Asap'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnChange = mlbChange
    end
    object Panel1: TPanel
      Left = 136
      Top = 164
      Width = 561
      Height = 25
      BevelOuter = bvNone
      TabOrder = 3
      object Label2: TLabel
        Left = 0
        Top = 2
        Width = 193
        Height = 22
        Caption = 'Match being played on'
      end
      object Label3: TLabel
        Left = 199
        Top = 2
        Width = 56
        Height = 22
        Caption = 'Label3'
        Font.Charset = ANSI_CHARSET
        Font.Color = 8454143
        Font.Height = -19
        Font.Name = 'Asap'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object hostCityI: TImage
        Left = 261
        Top = 5
        Width = 22
        Height = 15
        AutoSize = True
        Center = True
        Proportional = True
        Stretch = True
      end
      object Label4: TLabel
        Left = 289
        Top = 2
        Width = 56
        Height = 22
        Cursor = crHandPoint
        Caption = 'Label4'
        Font.Charset = ANSI_CHARSET
        Font.Color = 8454143
        Font.Height = -19
        Font.Name = 'Asap'
        Font.Style = [fsBold]
        ParentFont = False
        OnClick = Label4Click
      end
      object Label5: TLabel
        Left = 341
        Top = 2
        Width = 89
        Height = 22
        Caption = ' - capacity:'
      end
      object Label6: TLabel
        Left = 431
        Top = 2
        Width = 56
        Height = 22
        Caption = 'Label6'
        Font.Charset = ANSI_CHARSET
        Font.Color = 8454143
        Font.Height = -19
        Font.Name = 'Asap'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
    object g2B: TButton
      Left = 425
      Top = 129
      Width = 88
      Height = 29
      Caption = '+Goal'
      Style = bsSplitButton
      TabOrder = 4
      OnClick = Addagoal2Click
      OnDropDownClick = g2BDropDownClick
    end
    object g1B: TButton
      Left = 331
      Top = 129
      Width = 88
      Height = 29
      Caption = '+Goal'
      Style = bsSplitButton
      TabOrder = 5
      OnClick = Addagoal1Click
      OnDropDownClick = g1BDropDownClick
    end
  end
  object refT: TTimer
    Enabled = False
    OnTimer = refTTimer
    Left = 808
    Top = 232
  end
  object gM2: TPopupMenu
    Images = F1.ImageList1
    Left = 528
    Top = 136
    object Addagoal2: TMenuItem
      Caption = 'Add a goal'
      OnClick = Addagoal2Click
    end
    object Cancelagoal2: TMenuItem
      Caption = 'Cancel a goal'
      OnClick = Cancelagoal2Click
    end
  end
  object gM1: TPopupMenu
    Images = F1.ImageList1
    Left = 304
    Top = 136
    object Addagoal1: TMenuItem
      Caption = 'Add a goal'
      OnClick = Addagoal1Click
    end
    object Cancelagoal1: TMenuItem
      Caption = 'Cancel a goal'
      OnClick = Cancelagoal1Click
    end
  end
end
