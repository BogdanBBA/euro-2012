unit teams;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, PngImage;

type
  TFT = class(TForm)
    Panel0: TPanel;
    flagImg: TImage;
    Label1: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    Panel6: TPanel;
    Panel7: TPanel;
    Panel8: TPanel;
    exitB: TPanel;
    Panel9: TPanel;
    Panel10: TPanel;
    Panel11: TPanel;
    Panel12: TPanel;
    Panel13: TPanel;
    Panel14: TPanel;
    Panel15: TPanel;
    Panel16: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    colI: TImage;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Panel17: TPanel;
    m1t1L: TLabel;
    m1t1I: TImage;
    scoreL1: TLabel;
    m1t2I: TImage;
    m1t2L: TLabel;
    Panel18: TPanel;
    mIDL1: TLabel;
    dateL1: TLabel;
    stadiumL1: TLabel;
    Panel19: TPanel;
    mIDL2: TLabel;
    dateL2: TLabel;
    stadiumL2: TLabel;
    Panel20: TPanel;
    m2t1L: TLabel;
    m2t1I: TImage;
    scoreL2: TLabel;
    m2t2I: TImage;
    m2t2L: TLabel;
    Panel21: TPanel;
    mIDL3: TLabel;
    dateL3: TLabel;
    stadiumL3: TLabel;
    Panel22: TPanel;
    m3t1L: TLabel;
    m3t1I: TImage;
    scoreL3: TLabel;
    m3t2I: TImage;
    m3t2L: TLabel;
    procedure FormShow(Sender: TObject);
    procedure exitBClick(Sender: TObject);
    procedure exitBMouseEnter(Sender: TObject);
    procedure exitBMouseLeave(Sender: TObject);
    procedure Panel1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure m1t1LClick(Sender: TObject);
    procedure mIDL1Click(Sender: TObject);
    procedure stadiumL1Click(Sender: TObject);
    procedure scoreL1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FT: TFT;
  xt: byte;

implementation

{$R *.dfm}

uses DataTypes, functii, Euro12_pas, venues, matches;

procedure TFT.exitBClick(Sender: TObject);
begin FT.Close end;

procedure TFT.exitBMouseEnter(Sender: TObject);
begin
  if Sender is TLabel then TLabel(Sender).Font.Color:=clYellow
  else if Sender is TPanel then TPanel(Sender).Font.Color:=clYellow
end;

procedure TFT.exitBMouseLeave(Sender: TObject);
begin
  if Sender is TLabel then TLabel(Sender).Font.Color:=clWhite
  else if Sender is TPanel then TPanel(Sender).Font.Color:=clWhite
end;

procedure TFT.FormCreate(Sender: TObject);
var i, j: word;
begin
  for i:=1 to 3 do for j:=1 to 2 do
    TImage(FindComponent('m'+inttostr(i)+'t'+inttostr(j)+'I')).Picture.RegisterFileFormat('.PNG', 'Port Netw Graph', TPngImage);
end;

procedure TFT.FormShow(Sender: TObject);
var i: word;
begin
  for i:=1 to ne do TPanel(FindComponent('Panel'+inttostr(i))).Caption:=t[i-1].Name;
  Panel1Click(Panel1)
end;

procedure TFT.m1t1LClick(Sender: TObject);
var i: word;
begin
  if TLabel(Sender).Cursor=crHandPoint then
    for i:=1 to 16 do
      if TPanel(FindComponent('Panel'+inttostr(i))).Caption=TLabel(Sender).Caption then
        begin Panel1Click(TPanel(FindComponent('Panel'+inttostr(i)))); exit end
end;

procedure TFT.mIDL1Click(Sender: TObject);
begin
  FM.Show; FM.mlb.ItemIndex:=strtoint(TLabel(Sender).Caption)-1; FM.mlbChange(Sender)
end;

procedure TFT.Panel1Click(Sender: TObject);
var i, x, x0, wi, hi: word; mIDl: TStringList;
begin
  for i:=0 to ne-1 do if Sender=TPanel(FindComponent('Panel'+inttostr(i+1))) then
    begin x:=i; x0:=x; TPanel(FindComponent('Panel'+inttostr(i+1))).Font.Style:=[fsBold] end
  else TPanel(FindComponent('Panel'+inttostr(i+1))).Font.Style:=TPanel(FindComponent('Panel'+inttostr(i+1))).Font.Style-[fsBold];
  //
  if fileexists('photos\_'+t[x].ShortName+'.png') then flagImg.Picture.LoadFromFile('photos\_'+t[x].ShortName+'.png') else flagImg.Picture.LoadFromFile('photos\_UNK.png');
  Label1.Caption:=t[x].Name; Label6.Caption:=t[x].Nickname; Label10.Caption:=inttostr(x+1)+' ("'+t[x].ShortName+'")';
  //
  wi:=colI.Width; hi:=colI.Height;
  colI.Canvas.Brush.Color:=HtmlToColor(t[x].Color1); colI.Canvas.FillRect(Rect(0, 0, wi div 2, hi));
  colI.Canvas.Brush.Color:=HtmlToColor(t[x].Color2); colI.Canvas.FillRect(Rect(wi div 2, 0, wi, hi));
  //
  mIDl:=TStringList.Create;
  for i:=0 to 23 do if (m[i].Team1=t[x].ShortName) or (m[i].Team2=t[x].ShortName) then mIDl.Add(inttostr(i));
  if mIDl.Count<>3 then begin showmessage('ERROR: Number of group stage matches for team is not 3 (it''s now '+inttostr(mIDl.Count)+', should be 3)'); exit end;
  for i:=1 to 3 do
    begin
      x:=strtoint(mIDL[i-1]); TLabel(FindComponent('mIDL'+inttostr(i))).Caption:=inttostr(x+1);

      TLabel(FindComponent('dateL'+inttostr(i))).Caption:=formatdatetime('ddd d/mmm, h:nn', m[x].When);
      TLabel(FindComponent('stadiumL'+inttostr(i))).Caption:=m[x].City;
      TLabel(FindComponent('m'+inttostr(i)+'t1L')).Caption:=TeamFullNameByID(m[x].Team1);
      TLabel(FindComponent('m'+inttostr(i)+'t2L')).Caption:=TeamFullNameByID(m[x].Team2);
      TLabel(FindComponent('scoreL'+inttostr(i))).Caption:=MatchScore(x);

      if fileexists('photos\'+m[x].Team1+'.png') then TImage(FindComponent('m'+inttostr(i)+'t1I')).Picture.LoadFromFile('photos\'+m[x].Team1+'.png') else TImage(FindComponent('m'+inttostr(i)+'t1I')).Picture.LoadFromFile('photos\UNK.png');
      if fileexists('photos\'+m[x].Team2+'.png') then TImage(FindComponent('m'+inttostr(i)+'t2I')).Picture.LoadFromFile('photos\'+m[x].Team2+'.png') else TImage(FindComponent('m'+inttostr(i)+'t2I')).Picture.LoadFromFile('photos\UNK.png');

      if m[x].Team1=t[x0].ShortName then begin TLabel(FindComponent('m'+inttostr(i)+'t1L')).Cursor:=crDefault; TLabel(FindComponent('m'+inttostr(i)+'t2L')).Cursor:=crHandPoint end
      else begin TLabel(FindComponent('m'+inttostr(i)+'t1L')).Cursor:=crHandPoint; TLabel(FindComponent('m'+inttostr(i)+'t2L')).Cursor:=crDefault end;
    end
end;

procedure TFT.scoreL1Click(Sender: TObject);
begin mIDL1Click(FindComponent('mIDL'+TLabel(Sender).Name[7])) end;

procedure TFT.stadiumL1Click(Sender: TObject);
var k: word;
begin
  FVen.Show;
  for k:=0 to nv-1 do if TLabel(Sender).Caption=v[k].City then break;
  FVen.Panel1Click(FVen.FindComponent('Panel'+inttostr(k+1)))
end;

end.
