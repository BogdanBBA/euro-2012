unit MatchTime;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, PngImage, DataTypes, Menus, DateUtils;

type
  TFMT = class(TForm)
    Panel9: TPanel;
    t1L: TLabel;
    exitB: TPanel;
    t1I: TImage;
    col1I: TImage;
    refMlbB: TButton;
    mlb: TComboBox;
    scoreL: TLabel;
    t2L: TLabel;
    t2I: TImage;
    col2I: TImage;
    Label7: TLabel;
    timeL: TLabel;
    Panel1: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    hostCityI: TImage;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    g2B: TButton;
    g1B: TButton;
    dateL: TLabel;
    onL: TLabel;
    refT: TTimer;
    gM2: TPopupMenu;
    Addagoal2: TMenuItem;
    Cancelagoal2: TMenuItem;
    gM1: TPopupMenu;
    Addagoal1: TMenuItem;
    Cancelagoal1: TMenuItem;
    procedure g2BDropDownClick(Sender: TObject);
    procedure refMlbBClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure mlbChange(Sender: TObject);
    procedure exitBMouseEnter(Sender: TObject);
    procedure exitBMouseLeave(Sender: TObject);
    procedure exitBClick(Sender: TObject);
    procedure refTTimer(Sender: TObject);
    procedure scoreLClick(Sender: TObject);
    procedure t1LClick(Sender: TObject);
    procedure Label4Click(Sender: TObject);
    procedure g1BDropDownClick(Sender: TObject);
    procedure Addagoal1Click(Sender: TObject);
    procedure Addagoal2Click(Sender: TObject);
    procedure Cancelagoal1Click(Sender: TObject);
    procedure Cancelagoal2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FMT: TFMT;
  mx: word;
  cM: TMatch;

implementation

{$R *.dfm}

uses functii, Euro12_pas, matches, teams, venues;

procedure TFMT.g1BDropDownClick(Sender: TObject);
begin gM1.Popup(FMT.Left+g1B.Left+g1B.Width-8, FMT.Top+g1B.Top+g1B.Height-4) end;

procedure TFMT.g2BDropDownClick(Sender: TObject);
begin gM2.Popup(FMT.Left+g2B.Left+g2B.Width-8, FMT.Top+g2B.Top+g2B.Height-4) end;

procedure TFMT.Label4Click(Sender: TObject);
var i: word;
begin
  for i:=0 to nv-1 do if v[i].City=TLabel(Sender).Caption then break;
  FVen.Show; FVen.Panel1Click(FVen.FindComponent('Panel'+inttostr(i+1)))
end;

procedure TFMT.Addagoal1Click(Sender: TObject);
begin inc(cM.Goals1) end;

procedure TFMT.Addagoal2Click(Sender: TObject);
begin inc(cM.Goals2) end;

procedure TFMT.Cancelagoal1Click(Sender: TObject);
begin if cM.Goals1>0 then inc(cM.Goals1, -1) end;

procedure TFMT.Cancelagoal2Click(Sender: TObject);
begin if cM.Goals2>0 then inc(cM.Goals2, -1) end;

procedure TFMT.exitBClick(Sender: TObject);
begin
  refT.Enabled:=false; FMT.Close
end;

procedure TFMT.exitBMouseEnter(Sender: TObject);
begin
  if Sender is TLabel then TLabel(Sender).Font.Color:=clYellow
  else if Sender is TPanel then TPanel(Sender).Font.Color:=clYellow
end;

procedure TFMT.exitBMouseLeave(Sender: TObject);
begin
  if Sender is TLabel then TLabel(Sender).Font.Color:=clWhite
  else if Sender is TPanel then TPanel(Sender).Font.Color:=clWhite
end;

procedure TFMT.FormCreate(Sender: TObject);
var i: word;
begin
  t1I.Picture.RegisterFileFormat('.PNG', 'port netw graph', TPngImage);
  t2I.Picture.RegisterFileFormat('.PNG', 'port netw graph', TPngImage);
  hostCityI.Picture.RegisterFileFormat('.PNG', 'port netw graph', TPngImage);
  mlb.Clear; for i:=0 to nm-1 do mlb.Items.Add('Match '+inttostr(i)); refMlbBClick(FMT)
end;

procedure TFMT.FormShow(Sender: TObject);
begin
  refMlbBClick(FMT);
  if mlb.ItemIndex=-1 then begin mlb.ItemIndex:=0; mlbChange(FMT) end
end;

procedure TeamCol(var i: TImage; c1, c2: string);
begin
  //showmessage('hey: '+i.Name+' - '+c1+', '+c2);
  i.Canvas.Brush.Color:=HtmlToColor(c1); i.Canvas.FillRect(Rect(0, 0, i.Width div 2, i.Height));
  i.Canvas.Brush.Color:=HtmlToColor(c2); i.Canvas.FillRect(Rect(i.Width div 2, 0, i.Width, i.Height));
end;

procedure TFMT.mlbChange(Sender: TObject);
var i, x: word;
begin
  mx:=mlb.ItemIndex; cM:=m[mx];
  t1L.Caption:=TeamfullNamebyID(cM.Team1); t2L.Caption:=TeamfullNamebyID(cM.Team2); scoreL.Caption:=MatchScore(mx);
  if (cM.Disputed) and (now>inchour(cM.When,2)) then timeL.Caption:='Full Time' else timeL.Caption:='--:--';
  Addagoal1.Caption:='Add a goal for '+cM.Team1; Addagoal2.Caption:='Add a goal for '+cM.Team2; Cancelagoal1.Caption:='Cancel a goal for '+cM.Team1; Cancelagoal2.Caption:='Cancel a goal for '+cM.Team2;
  if fileexists('photos\_'+cM.Team1+'.png') then t1I.Picture.LoadFromFile('photos\_'+cM.Team1+'.png') else t1I.Picture.LoadFromFile('photos\_UNK.png');
  if fileexists('photos\_'+cM.Team2+'.png') then t2I.Picture.LoadFromFile('photos\_'+cM.Team2+'.png') else t2I.Picture.LoadFromFile('photos\_UNK.png');;
  for i:=0 to ne-1 do if t[i].ShortName=cM.Team1 then TeamCol(col1I, t[i].Color1, t[i].Color2) else if t[i].ShortName=cM.Team2 then TeamCol(col2I, t[i].Color1, t[i].Color2);
  for i:=0 to nv-1 do if v[i].City=cM.City then begin x:=i; break end;
  if fileexists('photos\'+v[x].Country+'.png') then hostCityI.Picture.LoadFromFile('photos\'+v[x].Country+'.png') else hostCityI.Picture.LoadFromFile('photos\UNK.png');
  Label3.Caption:=v[x].Name+','; Label4.Caption:=v[x].City; Label6.Caption:=inttostr(v[x].Capacity div 1000)+ThousandSeparator+formatfloat('000', v[x].Capacity mod 1000);
  hostCityI.Left:=Label3.Left+Label3.Width+6; Label4.Left:=hostCityI.Left+hostCityI.Width+6; Label5.Left:=Label4.Left+Label4.Width+6; Label6.Left:=Label5.Left+Label5.Width+6;
  Panel1.Width:=Label6.Left+Label6.Width+3; Panel1.Left:=Panel9.Width div 2 - Panel1.Width div 2;
  dateL.Caption:=formatdatetime('dddd, d mmmm yyyy, h:nn', cM.When); refT.Enabled:=true; refTTimer(mlb)
end;

procedure TFMT.refMlbBClick(Sender: TObject);
var i: word;
begin
  try
  for i:=0 to nm-1 do
    mlb.Items[i]:='Match '+inttostr(i+1)+': '+TeamFullNameByID(m[i].Team1)+'-'+TeamFullNameByID(m[i].Team2)
  except on E:Exception do showmessage('TFMT.refMlbBClick() ERROR, i='+inttostr(i)+' (match '+inttostr(i+1)+')'+dnl+E.ClassName+': '+E.Message) end;
end;

procedure TFMT.scoreLClick(Sender: TObject);
begin
  FM.show; FM.mlb.ItemIndex:=mx; FM.mlbChange(scoreL)
end;

procedure TFMT.t1LClick(Sender: TObject);
var i: word;
begin
  for i:=0 to ne-1 do if t[i].Name=TLabel(Sender).Caption then break;
  FT.Show; FT.Panel1Click(FT.FindComponent('Panel'+inttostr(i+1)))
end;

function FMatchTime(x: longint): string;
begin
  result := song_length(x*1000)
end;

procedure TFMT.refTTimer(Sender: TObject);
const su=1/(24*3600);
var ns: word;
begin
  //log(formatdatetime('yy/m/d h:nn:ss', now)+', game='+formatdatetime('yy/m/d h:nn', cM.When)+'-'+formatdatetime('yy/m/d h:nn', incminute(cM.When, 90+15)));
  if (cM.When<=now) and (now<=incminute(cM.When, 90+15)) then
    begin onL.Caption:='ON now'; onL.Font.Color:=clGreen end
    else begin onL.Caption:='Not ON now'; onL.Font.Color:=clRed end;
  //
  if onL.Font.Color=clRed then exit;
  scoreL.Caption:=inttostr(cM.Goals1)+'-'+inttostr(cM.Goals2);
  ns:=round(frac(now-cM.When)/su); log('ns='+inttostr(ns));
  timeL.Caption:=FMatchTime(ns)
end;

end.
